package com.xhu.atms.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * Created by bingge on 2019/3/22.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(value = "dev")
public class FooPropertiesTest {
    @Resource
    private FooProperties fooProperties;

    @Test
    public void testGetVersion(){
        System.out.println(fooProperties.getVersion());
    }

    @Test
    public void testGetKeyt(){
        System.out.println(fooProperties.getKeyt());
    }
}
