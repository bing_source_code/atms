package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Role;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class RoleMapperTest {
    @Resource(name = "roleMapper")
    private RoleMapper roleMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testCountRole(){
        Role role = new Role();
        int i = roleMapper.countRole(role);
        System.out.println(i);
    }

    @Test
    public void testSelectRole(){
        // 权限对象，包含查询条件
        Role role = new Role();
        /*Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        role.setPager(page);*/
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = roleMapper.countRole(role);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<Role> roles = roleMapper.selectRole(role);
        privMap.put("data",roles);
        String fields[] = new String[]{"code","msg","count","data","roleId",
                "roleName","roleCode","roleDesc","statusCd","statusDate",
                "createPerson","createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddRole(){
        Role role = new Role();
        role.setRoleName("测试角色");
        long roleCode = nextCodeMapper.selectNextCode("role");
        role.setRoleCode(roleCode);
        role.setRoleDesc("测试角色");
        role.setStatusCd(1L);
        role.setCreatePerson(31200001);
        int i = roleMapper.addRole(role);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testUpdateRole(){
        Role role = new Role();
        role.setRoleCode(6L);
        role.setRoleName("修改角色");
        role.setRoleDesc("修改角色");
        role.setStatusCd(1L);
        role.setUpdatePerson(31200001);
        int i = roleMapper.modifyRole(role);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteRole(){
        Role role = new Role();
        role.setRoleCode(6L);
        role.setUpdatePerson(31200001);
        int i = roleMapper.deleteRole(role);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteRoles(){
        Role role1 = new Role();
        role1.setUpdatePerson(31200001);
        role1.setRoleCode(1L);
        Role role2 = new Role();
        role2.setRoleCode(2L);
        role2.setUpdatePerson(31200001);
        Role role3 = new Role();
        role3.setRoleCode(3L);
        role3.setUpdatePerson(31200001);
        List<Role> roles = new ArrayList<>();
        roles.add(role1);
        roles.add(role2);
        roles.add(role3);
        int i = roleMapper.deleteRoles(roles);
        if (i != roles.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
