package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Document;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class DocumentMapperTest {
    @Resource(name = "documentMapper")
    private DocumentMapper documentMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testCountDocument(){
        Document document = new Document();
        int i = documentMapper.countDocument(document);
        System.out.println(i);
    }

    @Test
    public void testSelectDocument(){
        // 权限对象，包含查询条件
        Document document = new Document();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        document.setPager(page);
        document.setSortByStatusDate(true);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = documentMapper.countDocument(document);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<Document> documents = documentMapper.selectDocument(document);
        privMap.put("data",documents);
        String fields[] = new String[]{"code","msg","count","data","docId",
                "docName","docCode","docDesc","courseCode","courseName",
                "teacherCode","teacherName","docType","docTypeName",
                "uploadDate","uploadUrl","statusCd","statusName","statusDate",
                "createPerson","createPersonType","createDate",
                "updatePerson","updatePersonType","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddDocument(){
        Document document = new Document();
        document.setDocName("课后作业.doc");
        long documentCode = nextCodeMapper.selectNextCode("document");
        document.setDocCode(documentCode);
        document.setDocDesc("测试文档");
        document.setCourseCode(1L);
        document.setTeacherCode(31200901001L);
        document.setDocType(3L);
        document.setUploadUrl("/doc/" + document.getTeacherCode() + "/" +document.getDocCode());
        document.setStatusCd(1L);
        document.setCreatePerson(31200901001L);
        document.setCreatePersonType(2L);
        int i = documentMapper.addDocument(document);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testUpdateDocument(){
        Document document = new Document();
        document.setDocCode(2L);
        document.setDocName("课堂作业.doc");
        document.setDocDesc("测试文档");
        document.setCourseCode(1L);
        document.setTeacherCode(31200901001L);
        document.setDocType(3L);
        document.setStatusCd(1L);
        document.setUpdatePerson(31200901001L);
        document.setUpdatePersonType(2L);
        int i = documentMapper.modifyDocument(document);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteDocument(){
        Document document = new Document();
        document.setDocCode(2L);
        document.setUpdatePerson(31200901001L);
        document.setUpdatePersonType(2L);
        int i = documentMapper.deleteDocument(document);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteDocuments(){
        Document document1 = new Document();
        document1.setDocCode(2L);
        document1.setUpdatePerson(31200901001L);
        document1.setUpdatePersonType(2L);
        Document document2 = new Document();
        document2.setDocCode(1L);
        document2.setUpdatePerson(31200901001L);
        document2.setUpdatePersonType(2L);
        List<Document> documents = new ArrayList<>();
        documents.add(document1);
        documents.add(document2);
        int i = documentMapper.deleteDocuments(documents);
        if (i != documents.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
