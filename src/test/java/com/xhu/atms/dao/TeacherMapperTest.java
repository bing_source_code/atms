package com.xhu.atms.dao;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Teacher;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Teacher;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class TeacherMapperTest {
    @Resource(name = "teacherMapper")
    private TeacherMapper teacherMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private Account account;

    @Test
    public void testSelectByCode() {
        account.setAccountCode(31200901001L);
        account.setAccountPassword("asdf");
        Account teacher = teacherMapper.selectByCode(account);
        System.out.println(teacher);
    }

    @Test
    public void  testValidateInfo(){
        account.setAccountName("陈波");
        account.setAccountCode(31200901001L);
        account.setAccountDesc("陈波老师");
        int i = teacherMapper.validateInfo(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void  testResetPassword(){
        account.setAccountCode(31200901001L);
        account.setAccountPassword("456");
        int i = teacherMapper.resetPassword(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testSelectByTeacherCode(){
        account.setAccountCode(31200901001L);
        Teacher teacher = teacherMapper.selectByTeacherCode(account);
        System.out.println(teacher);
    }

    @Test
    public void testModifyPersonalInfo(){
        Teacher teacher = new Teacher();
        teacher.setAccountCode(31200901005L);
        teacher.setAccountName("测试修改教师");
        teacher.setAccountDesc("测试修改教师");
        teacher.setTeacherMobilePhoneNum("18076903045");
        teacher.setTeacherEmail("461691349@qq.com");
        teacher.setTeacherSchool("西华大学");
        teacher.setTeacherAcademy("计算机与软件工程学院");
        teacher.setTeacherMajor("计算机科学与技术专业");
        teacher.setUploadUrl("/upload/img/admin/31200004");
        teacher.setImgName("xxx.jpg");
        teacher.setStatusCd(1L);
        teacher.setUpdatePerson(31200901005L);
        teacher.setUpdatePersonType(2L);
        int i = teacherMapper.modifyPersonalInfo(teacher);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testModifyPassword(){
        account.setAccountCode(31200901001L);
        account.setOldAccountPassword("456");
        account.setAccountPassword("abc");
        int i = teacherMapper.modifyPassword(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testCountAccount(){
        Teacher teacher = new Teacher();
        int i = teacherMapper.countAccount(teacher);
        System.out.println(i);
    }

    @Test
    public void testSelectAccount(){
        // 权限对象，包含查询条件
        Teacher teacher = new Teacher();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        teacher.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = teacherMapper.countAccount(teacher);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        teacher.setFooProperties(fooProperties);
        List<Teacher> teachers = teacherMapper.selectAccount(teacher);
        privMap.put("data",teachers);
        String fields[] = new String[]{"code","msg","count","data","accountId",
                "accountName", "accountCode","accountDesc","teacherMobilePhoneNum",
                "teacherEmail", "teacherSchool", "teacherAcademy","teacherMajor",
                "uploadDate", "uploadUrl", "imgName", "statusCd","statusName", "statusDate",
                "createPerson", "createDate",
                "updatePerson","updatePersonType","updatePersonTypeName", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddAccount(){
        Teacher teacher = new Teacher();
        teacher.setAccountName("测试教师");
        long teacherCode = nextCodeMapper.selectNextCode("teacher");
        teacher.setAccountCode(teacherCode);
        teacher.setAccountPassword("abc");
        teacher.setAccountDesc("测试教师");
        teacher.setTeacherMobilePhoneNum("18224212902");
        teacher.setTeacherEmail("1782422187@qq.com");
        teacher.setTeacherSchool("西华大学");
        teacher.setTeacherAcademy("计算机与软件工程学院");
        teacher.setTeacherMajor("计算机科学与技术专业");
        teacher.setStatusCd(1L);
        teacher.setCreatePerson(31200001L);
        teacher.setFooProperties(fooProperties);
        int i = teacherMapper.addAccount(teacher);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteAccount(){
        Teacher teacher = new Teacher();
        teacher.setAccountCode(31200901005L);
        teacher.setUpdatePerson(31200001L);
        teacher.setUpdatePersonType(1L);
        int i = teacherMapper.deleteAccount(teacher);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteAccounts(){
        Teacher teacher1 = new Teacher();
        teacher1.setAccountCode(31200901001L);
        teacher1.setUpdatePerson(31200001L);
        teacher1.setUpdatePersonType(1L);
        Teacher teacher2 = new Teacher();
        teacher2.setAccountCode(31200901002L);
        teacher2.setUpdatePerson(31200001L);
        teacher2.setUpdatePersonType(1L);
        Teacher teacher3 = new Teacher();
        teacher3.setAccountCode(31200901003L);
        teacher3.setUpdatePerson(31200001L);
        teacher3.setUpdatePersonType(1L);
        List<Teacher> teachers = new ArrayList<>();
        teachers.add(teacher1);
        teachers.add(teacher2);
        teachers.add(teacher3);
        int i = teacherMapper.deleteAccounts(teachers);
        if (i != teachers.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
