package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Course;
import com.xhu.atms.entity.Page;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class CourseMapperTest {
    @Resource(name = "courseMapper")
    private CourseMapper courseMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testCountCourse(){
        Course course = new Course();
        int count = courseMapper.countCourse(course);
        System.out.println(count);
    }

    @Test
    public void testSelectCourse(){
        // 课程对象，包含查询条件
        Course course = new Course();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        course.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = courseMapper.countCourse(course);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<Course> courses = courseMapper.selectCourse(course);
        privMap.put("data",courses);
        String fields[] = new String[]{"code","msg","count","data","courseId","courseName",
                "courseCode","courseDesc","courseSchool","courseAcademy","courseMajor",
                "statusCd","statusName","statusDate","createPerson","createDate",
                "updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddCourse(){
        Course course = new Course();
        long courseCode = nextCodeMapper.selectNextCode("course");
        course.setCourseName("高等数学");
        course.setCourseCode(courseCode);
        course.setCourseDesc("高等数学");
        course.setCourseSchool("西华大学");
        course.setCourseAcademy("理学院");
        course.setCourseMajor("数学专业");
        course.setStatusCd(1L);
        course.setCreatePerson(31200001);
        int i = courseMapper.addCourse(course);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testUpdateCourse(){
        Course course = new Course();
        course.setCourseName("大学英语");
        course.setCourseCode(7L);
        course.setCourseDesc("大学英语");
        course.setCourseSchool("西华大学");
        course.setCourseAcademy("外国语学院");
        course.setCourseMajor("英语专业");
        course.setStatusCd(1L);
        course.setUpdatePerson(31200001);
        int i = courseMapper.modifyCourse(course);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteCourse(){
        Course course = new Course();
        course.setCourseCode(7L);
        course.setUpdatePerson(31200001);
        int i = courseMapper.deleteCourse(course);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteCourses(){
        Course course1 = new Course();
        course1.setCourseCode(1L);
        course1.setUpdatePerson(31200001);
        Course course2 = new Course();
        course2.setCourseCode(2L);
        course2.setUpdatePerson(31200001);
        List<Course> courses = new ArrayList<>();
        courses.add(course1);
        courses.add(course2);
        int i = courseMapper.deleteCourses(courses);
        if (i != courses.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
