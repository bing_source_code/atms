package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.AccRoleRel;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.AccRoleRel;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class AccRoleRelMapperTest {
	@Resource(name = "accRoleRelMapper")
	private AccRoleRelMapper accRoleRelMapper;

    @Test
    public void testCountAccRoleRel(){
        AccRoleRel accRoleRel = new AccRoleRel();
        int i = accRoleRelMapper.countAccRoleRel(accRoleRel);
        System.out.println(i);
    }

    @Test
    public void testSelectAccRoleRel(){
        // 权限对象，包含查询条件
        AccRoleRel accRoleRel = new AccRoleRel();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        accRoleRel.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = accRoleRelMapper.countAccRoleRel(accRoleRel);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<AccRoleRel> accRoleRels = accRoleRelMapper.selectAccRoleRel(accRoleRel);
        privMap.put("data",accRoleRels);
        String fields[] = new String[]{"code","msg","count","data","accId","accName",
                "roleId","roleName","relDesc","statusCd","statusName","statusDate",
                "createPerson","createDate","createPersonType","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddAccRoleRel(){
        AccRoleRel accRoleRel = new AccRoleRel();
        accRoleRel.setAccId(3120150901630L);
        accRoleRel.setRoleId(6L);
        accRoleRel.setAccType(3L);
        accRoleRel.setRelDesc("测试学生的测试角色");
        accRoleRel.setStatusCd(1L);
        accRoleRel.setCreatePerson(31200001L);
        accRoleRel.setCreatePersonType(1L);
        int i = accRoleRelMapper.addAccRoleRel(accRoleRel);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testUpdateAccRoleRel(){
        AccRoleRel accRoleRel = new AccRoleRel();
        accRoleRel.setAccId(3120150901630L);
        accRoleRel.setRoleId(6L);
        accRoleRel.setAccType(3L);
        accRoleRel.setRelDesc("测试账号角色");
        accRoleRel.setStatusCd(2L);
        accRoleRel.setUpdatePerson(31200001);
        int i = accRoleRelMapper.modifyAccRoleRel(accRoleRel);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteAccRoleRel(){
        AccRoleRel accRoleRel = new AccRoleRel();
        accRoleRel.setAccId(3120150901628L);
        accRoleRel.setRoleId(5L);
        accRoleRel.setAccType(3L);
        accRoleRel.setUpdatePerson(31200001);
        int i = accRoleRelMapper.deleteAccRoleRel(accRoleRel);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

}
