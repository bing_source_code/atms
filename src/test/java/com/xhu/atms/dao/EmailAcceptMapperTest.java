package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.EmailAccept;
import com.xhu.atms.entity.Page;
import net.sf.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class EmailAcceptMapperTest {
    @Resource(name = "emailAcceptMapper")
    private EmailAcceptMapper emailAcceptMapper;

    @Test
    public void testCountEmailAccept(){
        EmailAccept emailAccept = new EmailAccept();
        int i = emailAcceptMapper.countEmailAccept(emailAccept);
        System.out.println(i);
    }

    @Test
    public void testSelectEmailAccept(){
        // 发送邮件对象，包含查询条件
        EmailAccept emailAccept = new EmailAccept();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        emailAccept.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = emailAcceptMapper.countEmailAccept(emailAccept);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<EmailAccept> emailAccepts = emailAcceptMapper.selectEmailAccept(emailAccept);
        for (EmailAccept email : emailAccepts) {
            Assert.assertNotNull(email.getFromPersonType());
            System.out.println(email);
        }
        privMap.put("data",emailAccepts);
        String fields[] = new String[]{"code","msg","count","data","emailAcceptId",
                "emailAcceptCode","emailAcceptDesc","toPerson","toPersonType","toPersonTypeName",
                "fromPerson","fromPersonType","fromPersonTypeName","emailSubject", "emailBody",
                "emailAttachment","uploadDate","uploadUrl" ,"statusCd","statusName","statusDate",
                "createPerson","createPersonType","createPersonTypName","createDate",
                "updatePerson","updatePersonType","updatePersonTypeName","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddEmailAccept(){
        EmailAccept emailAccept = new EmailAccept();
        emailAccept.setEmailAcceptCode(2L);
        emailAccept.setEmailAcceptDesc("测试接收邮件");
        emailAccept.setToPerson(3120150901601L);
        emailAccept.setToPersonType(2L);
        emailAccept.setFromPerson(31200901001L);
        emailAccept.setFromPersonType(1L);
        emailAccept.setEmailSubject("测试接收邮件");
        emailAccept.setEmailBody("测试接收邮件");
        emailAccept.setUploadUrl("/upload/email/" + emailAccept.getEmailAcceptCode());
        emailAccept.setStatusCd(1L);
        emailAccept.setCreatePerson(31200901001L);
        emailAccept.setCreatePersonType(2L);
        int i = emailAcceptMapper.addEmailAccept(emailAccept);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testModifyEmailAccept(){
        EmailAccept emailAccept = new EmailAccept();
        emailAccept.setEmailAcceptCode(2L);
        emailAccept.setEmailAcceptDesc("测试修改接收邮件");
        emailAccept.setToPerson(3120150901621L);
        emailAccept.setToPersonType(2L);
        emailAccept.setFromPerson(31200901002L);
        emailAccept.setFromPersonType(1L);
        emailAccept.setEmailSubject("测试修改接收邮件");
        emailAccept.setEmailBody("测试修改接收邮件");
        emailAccept.setStatusCd(2L);
        emailAccept.setUpdatePerson(3120150901621L);
        emailAccept.setUpdatePersonType(2L);
        int i = emailAcceptMapper.modifyEmailAccept(emailAccept);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testModifyEmailAcceptsStatus(){
        EmailAccept emailAccept1 = new EmailAccept();
        emailAccept1.setEmailAcceptCode(2L);
        emailAccept1.setStatusCd(2L);
        emailAccept1.setUpdatePerson(31200901001L);
        emailAccept1.setUpdatePersonType(1L);
        EmailAccept emailAccept2 = new EmailAccept();
        emailAccept2.setEmailAcceptCode(19L);
        emailAccept2.setStatusCd(2L);
        emailAccept2.setUpdatePerson(31200901001L);
        emailAccept2.setUpdatePersonType(1L);
        List<EmailAccept> emailAccepts = new ArrayList<>();
        emailAccepts.add(emailAccept1);
        emailAccepts.add(emailAccept2);
        int i = emailAcceptMapper.modifyEmailAcceptsStatus(emailAccepts);
        if (i != emailAccepts.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteEmailAccept(){
        EmailAccept emailAccept = new EmailAccept();
        emailAccept.setEmailAcceptCode(3L);
        emailAccept.setUpdatePerson(3120150901620L);
        emailAccept.setUpdatePersonType(2L);
        int i = emailAcceptMapper.deleteEmailAccept(emailAccept);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteEmailAccepts(){
        EmailAccept emailAccept1 = new EmailAccept();
        emailAccept1.setEmailAcceptCode(2L);
        emailAccept1.setUpdatePerson(3120150901620L);
        emailAccept1.setUpdatePersonType(2L);
        EmailAccept emailAccept2 = new EmailAccept();
        emailAccept2.setEmailAcceptCode(3L);
        emailAccept2.setUpdatePerson(3120150901621L);
        emailAccept2.setUpdatePersonType(2L);
        EmailAccept emailAccept3 = new EmailAccept();
        emailAccept3.setEmailAcceptCode(4L);
        emailAccept3.setUpdatePerson(3120150901621L);
        emailAccept3.setUpdatePersonType(2L);
        List<EmailAccept> emailAccepts = new ArrayList<>();
        emailAccepts.add(emailAccept1);
        emailAccepts.add(emailAccept2);
        emailAccepts.add(emailAccept3);
        int i = emailAcceptMapper.deleteEmailAccepts(emailAccepts);
        if (i != emailAccepts.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
