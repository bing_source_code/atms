package com.xhu.atms.dao;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Student;
import com.xhu.atms.entity.Student;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class StudentMapperTest {
    @Resource(name = "studentMapper")
    private StudentMapper studentMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private Account account;

    @Test
    public void testSelectByCode() {
        account.setAccountCode(3120150901601L);
        account.setAccountPassword("abc");
        Account student = studentMapper.selectByCode(account);
        System.out.println(student);
    }

    @Test
    public void testAddStudent(){
        Student student = new Student();
        student.setAccountName("测试学生");
        long studentCode = nextCodeMapper.selectNextCode("student");
        student.setAccountCode(studentCode);
        student.setAccountPassword("abc");
        student.setAccountDesc("测试学生");
        student.setStudentSex("男");
        student.setStudentMobilePhoneNum("18224212902");
        student.setStudentEmail("167985156@163.com");
        student.setStudentSchool("西华大学");
        student.setStudentAcademy("计算机与软件工程学院");
        student.setStudentMajor("计算机科学与技术专业");
        student.setStudentClass("计科15-6班");
        student.setStatusCd(1L);
        student.setCreatePerson(studentCode);
        student.setCreatePersonType(2L);
        student.setFooProperties(account.getFooProperties());
        int i = studentMapper.addStudent(student);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void  testValidateInfo(){
        account.setAccountName("张三");
        account.setAccountCode(3120150901601L);
        account.setAccountDesc("张三学生");
        int i = studentMapper.validateInfo(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void  testResetPassword(){
        account.setAccountCode(3120150901601L);
        account.setAccountPassword("456");
        int i = studentMapper.resetPassword(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testSelectByStudentCode(){
        account.setAccountCode(3120150901601L);
        Student student = studentMapper.selectByStudentCode(account);
        System.out.println(student);
    }

    @Test
    public void testModifyPersonalInfo(){
        Student student = new Student();
        student.setAccountCode(3120150901606L);
        student.setAccountName("测试修改学生");
        student.setAccountDesc("测试修改学生");
        student.setStudentSex("女");
        student.setStudentMobilePhoneNum("18224212902");
        student.setStudentEmail("1316491753@qq.com");
        student.setStudentSchool("清华大学");
        student.setStudentAcademy("经济学院");
        student.setStudentMajor("经济专业");
        student.setStudentClass("经济15-1班");
        student.setStatusCd(1L);
        student.setUploadUrl("/upload/img/3120150901606");
        student.setImgName("xxx.jpg");
        student.setUpdatePerson(3120150901606L);
        student.setUpdatePersonType(1L);
        int i = studentMapper.modifyPersonalInfo(student);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testModifyPassword(){
        account.setAccountCode(3120150901601L);
        account.setOldAccountPassword("456");
        account.setAccountPassword("abc");
        int i = studentMapper.modifyPassword(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testCountAccount(){
        Student student = new Student();
        int i = studentMapper.countAccount(student);
        System.out.println(i);
    }

    @Test
    public void testSelectAccount(){
        // 权限对象，包含查询条件
        Student student = new Student();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        student.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = studentMapper.countAccount(student);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        student.setFooProperties(fooProperties);
        List<Student> students = studentMapper.selectAccount(student);
        privMap.put("data",students);
        String fields[] = new String[]{"code","msg","count","data","accountId",
                "accountName", "accountCode","accountDesc","studentSex",
                "studentMobilePhoneNum", "studentEmail","studentSchool", "studentAcademy",
                "studentMajor", "studentClass","uploadDate","uploadUrl","imgName","statusCd",
                "statusName", "statusDate", "createPerson", "createPersonType","createPersonTypeName",
                "createDate", "updatePerson","updatePersonType","updatePersonTypeName", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testDeleteAccount(){
        Student student = new Student();
        student.setAccountCode(3120150901606L);
        student.setUpdatePerson(31200001L);
        student.setUpdatePersonType(1L);
        int i = studentMapper.deleteAccount(student);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteAccounts(){
        Student student1 = new Student();
        student1.setAccountCode(3120150901601L);
        student1.setUpdatePerson(31200001L);
        student1.setUpdatePersonType(1L);
        Student student2 = new Student();
        student2.setAccountCode(3120150901602L);
        student2.setUpdatePerson(31200001L);
        student2.setUpdatePersonType(2L);
        Student student3 = new Student();
        student3.setAccountCode(3120150901603L);
        student3.setUpdatePerson(31200001L);
        student3.setUpdatePersonType(1L);
        List<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        int i = studentMapper.deleteAccounts(students);
        if (i != students.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
