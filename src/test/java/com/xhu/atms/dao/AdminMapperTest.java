package com.xhu.atms.dao;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Admin;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Admin;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class AdminMapperTest {
    @Resource(name = "adminMapper")
    private AdminMapper adminMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private Account account;

    @Test
    public void testSelectByCode() {
        account.setAccountCode(31200001L);
        account.setAccountPassword("123");
        Account admin = adminMapper.selectByCode(account);
        System.out.println(admin);
    }

    @Test
    public void  testValidateInfo(){
        account.setAccountName("admin");
        account.setAccountCode(31200001L);
        account.setAccountDesc("超级管理员");
        int i = adminMapper.validateInfo(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void  testResetPassword(){
        account.setAccountCode(31200001L);
        account.setAccountPassword("456");
        int i = adminMapper.resetPassword(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testSelectByAdminCode(){
        account.setAccountCode(31200001L);
        Admin admin = adminMapper.selectByAdminCode(account);
        System.out.println(admin);
    }

    @Test
    public void testModifyPersonalInfo(){
        Admin admin = new Admin();
        admin.setAccountCode(31200004L);
        admin.setAccountName("测试修改管理员");
        admin.setAccountDesc("测试修改管理员");
        admin.setUploadUrl("/images/admin/31200004");
        admin.setImgName("xxx.jpg");
        admin.setStatusCd(1L);
        admin.setUpdatePerson(31200004);
        admin.setFooProperties(fooProperties);
        int i = adminMapper.modifyPersonalInfo(admin);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testModifyPassword(){
        account.setAccountCode(31200004L);
        account.setOldAccountPassword("abc");
        account.setAccountPassword("456");
        int i = adminMapper.modifyPassword(account);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testCountAccount(){
        Admin admin = new Admin();
        int i = adminMapper.countAccount(admin);
        System.out.println(i);
    }

    @Test
    public void testSelectAccount(){
        // 权限对象，包含查询条件
        Admin admin = new Admin();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        admin.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = adminMapper.countAccount(admin);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        admin.setFooProperties(fooProperties);
        List<Admin> admins = adminMapper.selectAccount(admin);
        privMap.put("data",admins);
        String fields[] = new String[]{"code","msg","count","data","accountId",
                "accountName","accountCode","accountDesc","uploadDate",
                "uploadUrl","imgName","statusName","statusDate",
                "createPerson","createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddAccount(){
        Admin admin = new Admin();
        admin.setAccountName("测试管理员");
        Long adminCode = nextCodeMapper.selectNextCode("admin");
        admin.setAccountCode(adminCode);
        admin.setAccountPassword("abc");
        admin.setAccountDesc("测试管理员");
        admin.setStatusCd(1L);
        admin.setCreatePerson(adminCode.intValue());
        admin.setFooProperties(fooProperties);
        int i = adminMapper.addAccount(admin);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteAccount(){
        Admin admin = new Admin();
        admin.setAccountCode(31200004L);
        admin.setUpdatePerson(31200004);
        int i = adminMapper.deleteAccount(admin);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteAccounts(){
        Admin admin1 = new Admin();
        admin1.setAccountCode(31200001L);
        admin1.setUpdatePerson(31200001);
        Admin admin2 = new Admin();
        admin2.setAccountCode(31200002L);
        admin2.setUpdatePerson(31200002);
        Admin admin3 = new Admin();
        admin3.setAccountCode(31200003L);
        admin3.setUpdatePerson(31200003);
        List<Admin> admins = new ArrayList<>();
        admins.add(admin1);
        admins.add(admin2);
        admins.add(admin3);
        int i = adminMapper.deleteAccounts(admins);
        if (i != admins.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
