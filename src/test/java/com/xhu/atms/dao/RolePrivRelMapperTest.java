package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Role;
import com.xhu.atms.entity.RolePrivRel;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class RolePrivRelMapperTest {
    @Resource(name = "rolePrivRelMapper")
    private RolePrivRelMapper rolePrivRelMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testCountRolePrivRel(){
        RolePrivRel rolePrivRel = new RolePrivRel();
        int i = rolePrivRelMapper.countRolePrivRel(rolePrivRel);
        System.out.println(i);
    }

    @Test
    public void testSelectRolePrivRel(){
        // 权限对象，包含查询条件
        RolePrivRel rolePrivRel = new RolePrivRel();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        rolePrivRel.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = rolePrivRelMapper.countRolePrivRel(rolePrivRel);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<RolePrivRel> rolePrivRels = rolePrivRelMapper.selectRolePrivRel(rolePrivRel);
        privMap.put("data",rolePrivRels);
        String fields[] = new String[]{"code","msg","count","data","roleId",
                "roleName","privilegeId","privilegeName","statusCd","statusName",
                "statusDate","createPerson","createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddRolePrivRel(){
        RolePrivRel rolePrivRel = new RolePrivRel();
        rolePrivRel.setRoleId(6L);
        rolePrivRel.setRelDesc("测试");
        rolePrivRel.setPrivilegeId(2L);
        rolePrivRel.setStatusCd(1L);
        rolePrivRel.setCreatePerson(31200001);
        int i = rolePrivRelMapper.addRolePrivRel(rolePrivRel);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testUpdateRolePrivRel(){
        RolePrivRel rolePrivRel = new RolePrivRel();
        rolePrivRel.setRoleId(6L);
        rolePrivRel.setPrivilegeId(2L);
        rolePrivRel.setRelDesc("测试角色权限");
        rolePrivRel.setStatusCd(1L);
        rolePrivRel.setUpdatePerson(31200001);
        int i = rolePrivRelMapper.modifyRolePrivRel(rolePrivRel);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteRolePrivRel(){
        RolePrivRel rolePrivRel = new RolePrivRel();
        rolePrivRel.setRoleId(6L);
        rolePrivRel.setPrivilegeId(2L);
        rolePrivRel.setUpdatePerson(31200001);
        int i = rolePrivRelMapper.deleteRolePrivRel(rolePrivRel);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
