package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.EmailSend;
import net.sf.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class EmailSendMapperTest {
    @Resource(name = "emailSendMapper")
    private EmailSendMapper emailSendMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testCountEmailSend(){
        EmailSend emailSend = new EmailSend();
        int i = emailSendMapper.countEmailSend(emailSend);
        System.out.println(i);
    }

    @Test
    public void testSelectEmailSendByCode(){
        EmailSend emailSend = new EmailSend();
        emailSend.setEmailSendCode(5L);
        EmailSend downloadEmailSend = emailSendMapper.selectEmailSendByCode(emailSend);
        System.out.println(downloadEmailSend);
    }

    @Test
    public void testSelectEmailSend(){
        // 发送邮件对象，包含查询条件
        EmailSend emailSend = new EmailSend();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        emailSend.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = emailSendMapper.countEmailSend(emailSend);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<EmailSend> emailSends = emailSendMapper.selectEmailSend(emailSend);
        for (EmailSend email : emailSends) {
            Assert.assertNotNull(email.getFromPersonType());
            System.out.println(email);
        }
        privMap.put("data",emailSends);
        String fields[] = new String[]{"code","msg","count","data","emailSendId",
                "emailSendCode","emailSendDesc","toPerson","toPersonType","toPersonTypeName",
                "fromPerson","fromPersonType","fromPersonTypeName","emailSubject",
                "emailBody","emailAttachment", "statusCd","statusName","statusDate",
                "createPerson","createPersonType","createPersonTypName","createDate",
                "updatePerson","updatePersonType","updatePersonTypeName","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddEmailSend(){
        EmailSend emailSend = new EmailSend();
        long emailSendCode = nextCodeMapper.selectNextCode("email_send");
        emailSend.setEmailSendCode(emailSendCode);
        emailSend.setEmailSendDesc("测试发送邮件3");
        emailSend.setToPerson(3120150901620L);
        emailSend.setToPersonType(2L);
        emailSend.setFromPerson(31200901001L);
        emailSend.setFromPersonType(1L);
        emailSend.setEmailSubject("测试邮件4");
        emailSend.setEmailBody("测试给学生发送邮件4");
        emailSend.setStatusCd(2L);
        emailSend.setCreatePerson(31200901001L);
        emailSend.setCreatePersonType(1L);
        int i = emailSendMapper.addEmailSend(emailSend);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testModifyEmailSend(){
        EmailSend emailSend = new EmailSend();
        emailSend.setEmailSendCode(1L);
        emailSend.setEmailSendDesc("测试修改发送邮件");
        emailSend.setToPerson(3120150901621L);
        emailSend.setToPersonType(2L);
        emailSend.setFromPerson(31200901002L);
        emailSend.setFromPersonType(1L);
        emailSend.setEmailSubject("测试修改发送邮件");
        emailSend.setEmailBody("测试修改发送邮件");
        emailSend.setStatusCd(2L);
        emailSend.setUpdatePerson(31200901001L);
        emailSend.setUpdatePersonType(1L);
        int i = emailSendMapper.modifyEmailSend(emailSend);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteEmailSend(){
        EmailSend emailSend = new EmailSend();
        emailSend.setEmailSendCode(1L);
        emailSend.setUpdatePerson(31200901001L);
        emailSend.setUpdatePersonType(1L);
        int i = emailSendMapper.deleteEmailSend(emailSend);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeleteEmailSends(){
        EmailSend emailSend1 = new EmailSend();
        emailSend1.setEmailSendCode(2L);
        emailSend1.setUpdatePerson(31200901001L);
        emailSend1.setUpdatePersonType(1L);
        EmailSend emailSend2 = new EmailSend();
        emailSend2.setEmailSendCode(3L);
        emailSend2.setUpdatePerson(31200901001L);
        emailSend2.setUpdatePersonType(1L);
        EmailSend emailSend3 = new EmailSend();
        emailSend3.setEmailSendCode(4L);
        emailSend3.setUpdatePerson(31200901001L);
        emailSend3.setUpdatePersonType(1L);
        List<EmailSend> emailSends = new ArrayList<>();
        emailSends.add(emailSend1);
        emailSends.add(emailSend2);
        emailSends.add(emailSend3);
        int i = emailSendMapper.deleteEmailSends(emailSends);
        if (i != emailSends.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
