package com.xhu.atms.dao;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Privilege;
import net.bytebuddy.asm.Advice;
import net.sf.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.lang.annotation.Native;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class PrivilegeMapperTest {
    @Resource(name = "privilegeMapper")
    private PrivilegeMapper privilegeMapper;

    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testCountPriv(){
        Privilege privilege = new Privilege();
        int i = privilegeMapper.countPriv(privilege);
        System.out.println(i);
    }

    @Test
    public void testSelectPriv(){
        // 权限对象，包含查询条件
        Privilege privilege = new Privilege();
        Page page = new Page();
        page.setPage(1);
        page.setLimit(10);
        privilege.setPager(page);
        // Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table 组件默认规定的数据格式
        int code = 0;
        String msg = "success";
        int count = privilegeMapper.countPriv(privilege);// 查询带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);
        // 查询分页的一页内容
        List<Privilege> privileges = privilegeMapper.selectPriv(privilege);
        privMap.put("data",privileges);
        String fields[] = new String[]{"code","msg","count","data","privilegeId",
                "privilegeName","privilegeCode","privilegeDesc","url","statusCd",
                "statusDate","createPerson","createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        System.out.println(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    @Test
    public void testAddPrivilege(){
        Privilege privilege = new Privilege();
        privilege.setPrivilegeName("测试权限");
        long privilegeCode = nextCodeMapper.selectNextCode("privilege");
        privilege.setPrivilegeCode(privilegeCode);
        privilege.setPrivilegeDesc("测试权限");
        privilege.setUrl("/test/test.html");
        privilege.setStatusCd(1L);
        privilege.setCreatePerson(31200001);
        int i = privilegeMapper.addPrivilege(privilege);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testUpdatePrivilege(){
        Privilege privilege = new Privilege();
        privilege.setPrivilegeCode(15L);
        privilege.setPrivilegeName("修改权限");
        privilege.setPrivilegeDesc("修改权限");
        privilege.setUrl("/modify/modify.html");
        privilege.setStatusCd(1L);
        privilege.setUpdatePerson(31200001);
        int i = privilegeMapper.modifyPrivilege(privilege);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeletePrivilege(){
        Privilege privilege = new Privilege();
        privilege.setPrivilegeCode(15L);
        privilege.setUpdatePerson(31200001);
        int i = privilegeMapper.deletePrivilege(privilege);
        if (i != 1){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }

    @Test
    public void testDeletePrivileges(){
        Privilege privilege1 = new Privilege();
        privilege1.setUpdatePerson(31200001);
        privilege1.setPrivilegeCode(13L);
        Privilege privilege2 = new Privilege();
        privilege2.setPrivilegeCode(14L);
        privilege2.setUpdatePerson(31200001);
        Privilege privilege3 = new Privilege();
        privilege3.setPrivilegeCode(19L);
        privilege3.setUpdatePerson(31200001);
        List<Privilege> privileges = new ArrayList<>();
        privileges.add(privilege1);
        privileges.add(privilege2);
        privileges.add(privilege3);
        int i = privilegeMapper.deletePrivileges(privileges);
        if (i != privileges.size()){
            System.out.println("失败！");
        }else {
            System.out.println("成功！");
        }
    }
}
