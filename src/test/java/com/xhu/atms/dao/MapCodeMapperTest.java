package com.xhu.atms.dao;

import com.xhu.atms.entity.MapCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class MapCodeMapperTest {
    @Resource(name = "mapCodeMapper")
    private MapCodeMapper mapCodeMapper;

    @Test
    public void testSelectBy(){
        MapCode mapCode = new MapCode();
        mapCode.setColName("status_cd");
        mapCode.setTableName("privilege");
        List<MapCode> mapCodes = mapCodeMapper.selectMapCode(mapCode);
        for (MapCode code : mapCodes) {
            System.out.println(code);
        }
    }
}
