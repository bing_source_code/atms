package com.xhu.atms.dao;

import com.xhu.atms.entity.AccountPrivileges;
import com.xhu.atms.entity.PrivilegeMenu;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class AccountPrivilegesMapperTest {
    @Resource(name = "accountPrivilegesMapper")
    private AccountPrivilegesMapper accountPrivilegesMapper;

    @Test
    public void testSelectLoginPrivilege() {
        int i = accountPrivilegesMapper.selectLoginPrivilege(31200001L);
        if (i != 0){
            System.out.println("有登录权限");
        }else {
            System.out.println("没有登录权限");
        }
    }

	@Test
	public void testSelectAccountPrivileges() {
        List<PrivilegeMenu> privileges = accountPrivilegesMapper.selectAccountPrivileges(31200001L);
        for (PrivilegeMenu privilegeMenu : privileges) {
            System.out.println(privilegeMenu.getMenuType());
            for (AccountPrivileges accountPrivileges : privilegeMenu.getMenuItems()) {
                System.out.println(accountPrivileges);
            }
            System.out.println("-----------------------------------");
        }
    }
}
