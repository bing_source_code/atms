package com.xhu.atms.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan("com.xhu.atms.dao")
@ActiveProfiles(value = "dev")
public class NextCodeMapperTest {
    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Test
    public void testSelectNextCode(){
        long studentCode = nextCodeMapper.selectNextCode("privilege");
        System.out.println(studentCode);
    }
}
