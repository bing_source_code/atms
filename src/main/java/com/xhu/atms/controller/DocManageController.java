package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.*;
import com.xhu.atms.service.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@MultipartConfig
@RequestMapping("/DocManage")
public class DocManageController {
    @Resource
    private Common common;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private NextCodeService nextCodeService;

    @Resource
    private CourseService courseService;

    @Resource
    private TeacherService teacherService;

    @Resource
    private DocumentService docService;

    @RequestMapping("/manageDoc.html")
    public String manageDocument(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "DocManage/manageDoc";
    }

    @RequestMapping("/manageMyDoc.html")
    public String manageMyDoc(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "DocManage/manageMyDoc";
    }

    @RequestMapping("/lookDownloadDoc.html")
    public String lookDownloadDoc(Long privCode, Long docCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        if (docCode != null){
            request.setAttribute("docCode",docCode);
        }
        return "DocManage/lookDownloadDoc";
    }

    @RequestMapping("/lookDoc.html")
    public String lookDoc(Long docCode, HttpServletRequest request){
        if (docCode != null){
            request.setAttribute("docCode",docCode);
        }
        return "DocManage/lookDoc";
    }

    @RequestMapping("/selectCourse.do")
    public String doSelectCourse(Course course,HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        List<Course> courses = courseService.selectCourse(course);
        respMap.put("courses",courses);
        // 保留的属性名称数组
        String fields[] = new String[]{"courses","courseId","courseName","courseCode",
                "courseDesc","courseSchool","courseAcademy","courseMajor","statusCd",
                "statusName","statusDate","createPerson","createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);

        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/selectTeacher.do")
    public String doSelectTeacher(Teacher teacher,HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        List<Teacher> teachers = teacherService.selectAccount(teacher);
        respMap.put("teachers",teachers);
        // 保留的属性名称数组
        String fields[] = new String[]{"teachers","code","msg","count","data","accountId",
                "accountName", "accountCode","accountDesc","teacherMobilePhoneNum",
                "teacherEmail", "teacherSchool", "teacherAcademy","teacherMajor",
                "uploadDate", "uploadUrl", "imgName", "statusCd","statusName",
                "statusDate", "createPerson", "createDate", "updatePerson",
                "updatePersonType","updatePersonTypeName", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);

        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/selectDocType.do")
    public String doSelectDocType(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "doc_type", "document");
    }

    @RequestMapping("/selectDocStatusCd.do")
    public String doSelectDocStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "document");
    }

    @RequestMapping("/selectDoc.do")
    public String doSelectDoc(Document document, Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果docName=""，说明前台没有输入值，将docName置为空
        if (document.getDocName() != null && document.getDocName().trim().length() == 0){
            document.setDocName(null);
        }
        // 如果docDesc=""，说明前台没有输入值，将docDesc置为空
        if (document.getDocDesc() != null && document.getDocDesc().trim().length() == 0){
            document.setDocDesc(null);
        }

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = docService.countDocument(document);// 带条件的分页查询的总行数
        respMap.put("code",code);
        respMap.put("msg",msg);
        respMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        document.setPager(pager);
        List<Document> documents = docService.selectDocument(document);

        // 将此页的内容放入respMap中
        respMap.put("data", documents);
        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg","count","data",
                "docId","docName","docCode","docDesc","courseCode","courseName",
                "teacherCode","teacherName","docType","docTypeName","uploadDate",
                "uploadUrl","attachmentName","statusCd","statusName","statusDate",
                "createPerson","createPersonType","createPersonTypeName","createDate",
                "updatePerson","updatePersonType","updatePersonTypeName","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addDoc.do")
    public String doAddDoc(Document document, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (document == null
                || document.getDocName() == null
                || document.getDocName().trim().length() == 0
                || document.getCourseCode() == null
                || document.getTeacherCode() == null
                || document.getDocType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 设置文档资料的编号
        long documentCode = nextCodeService.selectNextCode("document");
        document.setDocCode(documentCode);
        document.setUploadUrl("/upload/doc/" + documentCode);
        int affectedRow = docService.addDocument(document);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加文档资料失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加文档资料成功
            writer.write("{\"actionFlag\":true,\"docCode\":" + documentCode + "}");
        }
        return null;
    }

    @RequestMapping("/uploadAddDoc.do")
    public String doUploadAddDoc( HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        long docCode = nextCodeService.selectNextCode("document");
        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg"};
        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        // 上传文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadDocDir() + "/" + docCode;
        // 上传目录
        File uploadDir = new File(realPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // 上传的文件
        Part uploadFilePart = request.getPart("file");
        // 上传的文件名
        String uploadFileName = common.getFileName(uploadFilePart);
        // 读写文件，将上传文件读入字节数组，然后写入到服务器上指定的文件
        respMap = common.readWriteFile(realPath, uploadFileName, uploadFilePart);
        // 将所有发往客户端的信息转化为json数组，并发往客户端
        common.writeRespJsonToClient(fields,respMap,writer);
        return null;
    }

    @RequestMapping("/uploadModifyDoc.do")
    public String doUploadModifyDoc( Long docCode, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg"};
        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        if (docCode == null) {
            respMap.put("code",-1);
            respMap.put("msg","docCode is null");
            common.writeRespJsonToClient(fields,respMap,writer);
            return null;
        }
        // 上传文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadDocDir() + "/" + docCode;
        // 上传目录
        File uploadDir = new File(realPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // 上传的文件
        Part uploadFilePart = request.getPart("file");
        // 上传的文件名
        String uploadFileName = common.getFileName(uploadFilePart);
        // 读写文件，将上传文件读入字节数组，然后写入到服务器上指定的文件
        respMap = common.readWriteFile(realPath, uploadFileName, uploadFilePart);
        // 将所有发往客户端的信息转化为json数组，并发往客户端
        common.writeRespJsonToClient(fields,respMap,writer);
        return null;
    }

    @RequestMapping("/downloadDoc.do")
    public String doDownloadDoc(Long docCode, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码
        response.setCharacterEncoding("utf-8");

        if (docCode == null){
            return null;
        }

        Document document = new Document();
        document.setDocCode(docCode);
        Document downloadDoc = docService.selectDocumentByCode(document);
        String downloadDocName = downloadDoc.getAttachmentName();
        String downloadFileName;

        // 获取用户客户端信息
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
            // 针对IE或者以IE为内核的浏览器：
            downloadFileName = URLEncoder.encode(downloadDocName, "UTF-8");
        } else {
            // 非IE浏览器的处理：
            downloadFileName = new String(downloadDocName.getBytes("UTF-8"), "ISO-8859-1");
        }

        // 下载文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadDocDir() + "/" + docCode;
        // 下载文件
        File downLoadFile = new File(realPath + File.separator + downloadDocName);
        // 下载文件长度
        long fileLength = downLoadFile.length();
        // 设置响应
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment; filename=" + downloadFileName);
        response.setHeader("Content-Length", String.valueOf(fileLength));

        // 获取输入流
        InputStream is = new FileInputStream(downLoadFile);
        // 获取输出流
        ServletOutputStream os = response.getOutputStream();

        int readLines = 0;
        byte[] bytes = new byte[1024];
        while ((readLines = is.read(bytes)) != -1){
            os.write(bytes,0,readLines);
        }
        os.flush();

        if (is != null) {
            is.close();
        }
        if (os != null) {
            os.close();
        }
        return null;
    }

    @RequestMapping("/modifyDoc.do")
    public String doModifyDoc(Document document, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (document == null
                || document.getDocName() == null
                || document.getDocName().trim().length() == 0
                || document.getCourseCode() == null
                || document.getTeacherCode() == null
                || document.getDocType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = docService.modifyDocument(document);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改文档资料失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改文档资料成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteDoc.do")
    public String doDeleteDoc(Document document, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (document == null
                || document.getDocCode() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = docService.deleteDocument(document);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除文档资料失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除文档资料成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteDocs.do")
    public String doDeleteDocs(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为文档资料列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Document document = (Document)JSONObject.toBean(jsonObject, Document.class);
            documents.add(document);
        }

        boolean actionValue = true;
        if (documents.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (Document document : documents) {
            // 如果文档资料编号为空，请求值错误
            if (document.getDocCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = docService.deleteDocuments(documents);// 受影响的行数
        if (affectedRow != documents.size()) {
            // 异步请求的响应：删除文档资料失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除文档资料成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
