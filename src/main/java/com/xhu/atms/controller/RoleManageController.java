package com.xhu.atms.controller;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Role;
import com.xhu.atms.service.NextCodeService;
import com.xhu.atms.service.RoleService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@RequestMapping("/RoleManage")
public class RoleManageController {
    @Resource
    private Common common;

    @Resource
    private RoleService roleService;

    @Resource
    private NextCodeService nextCodeService;

    @RequestMapping("/manageRole.html")
    public String manageRole(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "RoleManage/manageRole";
    }

    @RequestMapping("/selectRoleStatusCd.do")
    public String doSelectRoleStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "role");
    }

    @RequestMapping("/selectRole.do")
    public String doSelectRole(Role role, Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果roleName=""，说明前台没有输入值，将roleName置为空
        if (role.getRoleName() != null && role.getRoleName().trim().length() == 0){
            role.setRoleName(null);
        }
        // 如果roleDesc=""，说明前台没有输入值，将roleDesc置为空
        if (role.getRoleDesc() != null && role.getRoleDesc().trim().length() == 0){
            role.setRoleDesc(null);
        }

        // roleMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> roleMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = roleService.countRole(role);// 带条件的分页查询的总行数
        roleMap.put("code",code);
        roleMap.put("msg",msg);
        roleMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        role.setPager(pager);
        List<Role> roles = roleService.selectRole(role);

        // 将此页的内容放入roleMap中
        roleMap.put("data", roles);
        // 保留的属性名称数组
        String fields[] = new String[]{"code", "msg", "count", "data",
                "roleId","roleName", "roleCode", "roleDesc","statusCd" ,
                "statusName","statusDate","createPerson", "createDate",
                "updatePerson", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,roleMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addRole.do")
    public String doAddRole(Role role, HttpServletResponse response, HttpSession session) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (role == null
                || role.getRoleName() == null
                || role.getRoleName().trim().length() == 0) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 设置角色的编号
        long roleCode = nextCodeService.selectNextCode("role");
        role.setRoleCode(roleCode);
        int affectedRow = roleService.addRole(role);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加角色失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加角色成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyRole.do")
    public String doModifyRole(Role role, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (role == null
                || role.getRoleCode() == null
                || role.getRoleName() == null
                || role.getRoleName().trim().length() == 0){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = roleService.modifyRole(role);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改角色失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改角色成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteRole.do")
    public String doDeleteRole(Role role, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (role == null
                || role.getRoleCode() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = roleService.deleteRole(role);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除角色失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除角色成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteRoles.do")
    public String doDeleteRoles(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为角色列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<Role> roles = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Role role = (Role)JSONObject.toBean(jsonObject, Role.class);
            roles.add(role);
        }

        boolean actionValue = true;
        if (roles.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (Role role : roles) {
            // 如果角色编号为空，请求值错误
            if (role.getRoleCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = roleService.deleteRoles(roles);// 受影响的行数
        if (affectedRow != roles.size()) {
            // 异步请求的响应：删除角色失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除角色成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
