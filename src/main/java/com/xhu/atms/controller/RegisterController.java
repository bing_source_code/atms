package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.entity.AccRoleRel;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Student;
import com.xhu.atms.service.AccRoleRelService;
import com.xhu.atms.service.NextCodeService;
import com.xhu.atms.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by bingge on 2019/3/4.
 */
@Controller
public class RegisterController {
    @Resource(name = "nextCodeService")
    private NextCodeService nextCodeService;

    @Resource(name = "studentService")
    private StudentService studentService;

    @Resource(name = "accRoleRelService")
    private AccRoleRelService accRoleRelService;

    @Resource
    private FooProperties fooProperties;

    @RequestMapping("/register.html")
    public String register() {
        return "register";
    }

    @RequestMapping("/register.do")
    public String doRegister(Student student, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        boolean actionValue = true;
        if (student == null
                || student.getAccountName() == null
                || student.getAccountName().trim().length() == 0
                || student.getAccountPassword() == null
                || student.getAccountPassword().trim().length() == 0) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        long accountCode = nextCodeService.selectNextCode("student");
        student.setAccountCode(accountCode);// 设置学生编号
        student.setStatusCd(1L);// 设置学生状态为1（在用）
        student.setCreatePerson(accountCode);// 设置学生创建人
        student.setCreatePersonType(3L);// 设置学生创建人类型
        // 设置属性文件，包含密钥
        student.setFooProperties(fooProperties);
        int i = studentService.addStudent(student);
        if (i != 1) {
            // 异步请求的响应：注册失败
            writer.write("{\"actionFlag\":false}");
        } else {
            AccRoleRel accRoleRel = new AccRoleRel();
            accRoleRel.setAccId(accountCode);
            accRoleRel.setRoleId(3L);
            accRoleRel.setAccType(3L);
            accRoleRel.setRelDesc(student.getAccountName() + "的学生角色");
            accRoleRel.setStatusCd(1L);
            accRoleRel.setCreatePerson(accountCode);
            accRoleRel.setCreatePersonType(3L);
            // 给新注册的学生分配学生角色
            accRoleRelService.addAccRoleRel(accRoleRel);
            accRoleRel.setRoleId(5L);
            accRoleRel.setRelDesc(student.getAccountName() + "的登录角色");
            // 给新注册的学生分配登录角色
            accRoleRelService.addAccRoleRel(accRoleRel);
            // 异步请求的响应：注册成功，且将学生编号返回给用户
            writer.write("{\"actionFlag\":true,\"accountCode\":" + accountCode + "}");
        }
        return null;
    }
}
