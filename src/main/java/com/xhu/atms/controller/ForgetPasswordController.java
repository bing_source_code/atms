package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.entity.Account;
import com.xhu.atms.service.AdminService;
import com.xhu.atms.service.StudentService;
import com.xhu.atms.service.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by bingge on 2019/3/18.
 */
@Controller
public class ForgetPasswordController {
    @Resource(name = "adminService")
    private AdminService adminService;

    @Resource(name = "teacherService")
    private TeacherService teacherService;

    @Resource(name = "studentService")
    private StudentService studentService;

    @Resource
    private FooProperties fooProperties;

    @RequestMapping("/forgetPassword.html")
    public String forgerPassword(){
        return "forgetPassword";
    }

    @RequestMapping("/validateInfo.do")
    public String ValidateInfo(Account account, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        boolean actionValue = true;
        if (account == null
                || account.getAccountName() == null
                || account.getAccountName().trim().length() == 0
                || account.getAccountCode() == null
                || account.getAccountType() == null) {
            actionValue = false;
        }
        if (!actionValue){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int i = 0;
        switch (account.getAccountType().intValue()) {
            case 1:// 管理员
                i = adminService.validateInfo(account);
                break;
            case 2:// 教师
                i = teacherService.validateInfo(account);
                break;
            case 3:// 学生
                i = studentService.validateInfo(account);
                break;
            default:// 其他
                i = 0;
                break;
        }
        if (i != 1) {
            // 异步请求的响应：验证失败
            writer.write("{\"actionFlag\":false}");
        }else {
            // 异步请求的响应：验证成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/resetPassword.do")
    public String resetPassword(Account account, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        boolean actionValue = true;
        if (account == null
                || account.getAccountPassword() == null
                || account.getAccountPassword().trim().length() == 0
                || account.getAccountCode() == null
                || account.getAccountType() == null) {
            actionValue = false;
        }
        if (!actionValue){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int i = 0;
        // 设置属性文件，包含密钥
        account.setFooProperties(fooProperties);
        switch (account.getAccountType().intValue()) {
            case 1:// 管理员
                i = adminService.resetPassword(account);
                break;
            case 2:// 教师
                i = teacherService.resetPassword(account);
                break;
            case 3:// 学生
                i = studentService.resetPassword(account);
                break;
            default:// 其他
                i = 0;
                break;
        }
        if ( i != 1){
            // 异步请求的响应：重新设置密码失败
            writer.write("{\"actionFlag\":false}");
        }else {
            // 异步请求的响应：重新设置密码成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
