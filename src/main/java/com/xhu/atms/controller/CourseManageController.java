package com.xhu.atms.controller;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Course;
import com.xhu.atms.service.NextCodeService;
import com.xhu.atms.service.CourseService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@RequestMapping("/CourseManage")
public class CourseManageController {
    @Resource
    private Common common;

    @Resource
    private CourseService courseService;

    @Resource
    private NextCodeService nextCodeService;

    @RequestMapping("/manageCourse.html")
    public String manageCourse(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "CourseManage/manageCourse";
    }

    @RequestMapping("/selectCourseStatusCd.do")
    public String doSelectCourseStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "course");
    }

    @RequestMapping("/selectCourse.do")
    public String doSelectCourse(Course course, Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果courseName=""，说明前台没有输入值，将courseName置为空
        if (course.getCourseName() != null && course.getCourseName().trim().length() == 0){
            course.setCourseName(null);
        }
        // 如果courseDesc=""，说明前台没有输入值，将courseDesc置为空
        if (course.getCourseDesc() != null && course.getCourseDesc().trim().length() == 0){
            course.setCourseDesc(null);
        }

        // courseMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> courseMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = courseService.countCourse(course);// 带条件的分页查询的总行数
        courseMap.put("code",code);
        courseMap.put("msg",msg);
        courseMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        course.setPager(pager);
        List<Course> courses = courseService.selectCourse(course);

        // 将此页的内容放入courseMap中
        courseMap.put("data", courses);
        // 保留的属性名称数组
        String fields[] = new String[]{"code", "msg", "count", "data",
                "courseId","courseName", "courseCode", "courseDesc","courseSchool",
                "courseAcademy","courseMajor","statusCd" ,"statusName","statusDate",
                "createPerson", "createDate","updatePerson", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,courseMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addCourse.do")
    public String doAddCourse(Course course, HttpServletResponse response, HttpSession session) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (course == null
                || course.getCourseName() == null
                || course.getCourseName().trim().length() == 0) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 设置课程的编号
        long courseCode = nextCodeService.selectNextCode("course");
        course.setCourseCode(courseCode);
        int affectedRow = courseService.addCourse(course);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加课程失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加课程成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyCourse.do")
    public String doModifyCourse(Course course, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (course == null
                || course.getCourseCode() == null
                || course.getCourseName() == null
                || course.getCourseName().trim().length() == 0){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = courseService.modifyCourse(course);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改课程失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改课程成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteCourse.do")
    public String doDeleteCourse(Course course, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (course == null
                || course.getCourseCode() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = courseService.deleteCourse(course);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除课程失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除课程成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteCourses.do")
    public String doDeleteCourses(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为课程列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<Course> courses = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Course course = (Course)JSONObject.toBean(jsonObject, Course.class);
            courses.add(course);
        }

        boolean actionValue = true;
        if (courses.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (Course course : courses) {
            // 如果课程编号为空，请求值错误
            if (course.getCourseCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = courseService.deleteCourses(courses);// 受影响的行数
        if (affectedRow != courses.size()) {
            // 异步请求的响应：删除课程失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除课程成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
