package com.xhu.atms.controller;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.*;
import com.xhu.atms.service.PrivilegeService;
import com.xhu.atms.service.RolePrivRelService;
import com.xhu.atms.service.RoleService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@RequestMapping("/RolePrivRelManage")
public class RolePrivRelManageController {
    @Resource
    private Common common;

    @Resource
    private RolePrivRelService rolePrivRelService;

    @Resource
    private RoleService roleService;

    @Resource
    private PrivilegeService privilegeService;

    @RequestMapping("/manageRolePrivRel.html")
    public String manageRolePrivRel(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "RolePrivRelManage/manageRolePrivRel";
    }

    @RequestMapping("/selectRole.do")
    public String doSelectRole(Role role, HttpServletResponse response) throws IOException {
        return common.doSelectRole(role,response);
    }

    @RequestMapping("/selectPriv.do")
    public String doSelectPriv(Privilege privilege, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        List<Privilege> privileges = privilegeService.selectPriv(privilege);
        respMap.put("privileges",privileges);
        // 保留的属性名称数组
        String fields[] = new String[]{"privileges","privilegeId","privilegeName","privilegeCode",
                "privilegeDesc", "url","statusCd","statusName","statusDate",
                "createPerson", "createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);

        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/selectRolePrivRelStatusCd.do")
    public String doSelectRolePrivRelStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "role_priv_rel");
    }

    @RequestMapping("/selectRolePrivRel.do")
    public String doSelectRolePrivRel(RolePrivRel rolePrivRel, Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果rolePrivRelDesc=""，说明前台没有输入值，将rolePrivRelDesc置为空
        if (rolePrivRel.getRelDesc() != null && rolePrivRel.getRelDesc().trim().length() == 0){
            rolePrivRel.setRelDesc(null);
        }

        // rolePrivRelMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> rolePrivRelMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = rolePrivRelService.countRolePrivRel(rolePrivRel);// 带条件的分页查询的总行数
        rolePrivRelMap.put("code",code);
        rolePrivRelMap.put("msg",msg);
        rolePrivRelMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        rolePrivRel.setPager(pager);
        List<RolePrivRel> rolePrivRels = rolePrivRelService.selectRolePrivRel(rolePrivRel);

        // 将此页的内容放入rolePrivRelMap中
        rolePrivRelMap.put("data", rolePrivRels);
        // 保留的属性名称数组
        String fields[] = new String[]{"code", "msg", "count", "data",
                "roleId","roleName", "privilegeId", "privilegeName","relDesc",
                "statusCd","statusName","statusDate","createPerson", "createDate",
                "updatePerson", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,rolePrivRelMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addRolePrivRel.do")
    public String doAddRolePrivRel(RolePrivRel rolePrivRel, HttpServletResponse response, HttpSession session) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (rolePrivRel == null
                || rolePrivRel.getRoleId() == null
                || rolePrivRel.getPrivilegeId() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }
        
        int affectedRow = rolePrivRelService.addRolePrivRel(rolePrivRel);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加角色权限关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加角色权限关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyRolePrivRel.do")
    public String doModifyRolePrivRel(RolePrivRel rolePrivRel, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (rolePrivRel == null
                || rolePrivRel.getRoleId() == null
                || rolePrivRel.getPrivilegeId() == null){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = rolePrivRelService.modifyRolePrivRel(rolePrivRel);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改角色权限关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改角色权限关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteRolePrivRel.do")
    public String doDeleteRolePrivRel(RolePrivRel rolePrivRel, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (rolePrivRel == null
                || rolePrivRel.getRoleId() == null
                || rolePrivRel.getPrivilegeId() == null){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = rolePrivRelService.deleteRolePrivRel(rolePrivRel);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除角色权限关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除角色权限关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteRolePrivRels.do")
    public String doDeleteRolePrivRels(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为角色权限关系列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<RolePrivRel> rolePrivRels = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            RolePrivRel rolePrivRel = (RolePrivRel)JSONObject.toBean(jsonObject, RolePrivRel.class);
            rolePrivRels.add(rolePrivRel);
        }

        boolean actionValue = true;
        if (rolePrivRels.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (RolePrivRel rolePrivRel : rolePrivRels) {
            // 如果角色编号或者权限编号为空，请求值错误
            if (rolePrivRel.getRoleId() == null
                    || rolePrivRel.getPrivilegeId() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affecteRowSum = 0;// 受影响的总行数
        for (RolePrivRel rolePrivRel : rolePrivRels) {
            int affectedRow = rolePrivRelService.deleteRolePrivRel(rolePrivRel);// 受影响的行数
            if (affectedRow == 1){
                affecteRowSum += affectedRow;
            }else {
                break;
            }
        }
        if (affecteRowSum != rolePrivRels.size()) {
            // 异步请求的响应：删除角色权限关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除角色权限关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
