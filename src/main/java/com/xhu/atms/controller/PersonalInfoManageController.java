package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.*;
import com.xhu.atms.service.AdminService;
import com.xhu.atms.service.StudentService;
import com.xhu.atms.service.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by bingge on 2019/3/23.
 */
@Controller
@RequestMapping("/PersonalInfoManage")
public class PersonalInfoManageController {
    @Resource(name = "adminService")
    private AdminService adminService;

    @Resource(name = "teacherService")
    private TeacherService teacherService;

    @Resource(name = "studentService")
    private StudentService studentService;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private Common common;

    @RequestMapping("/modifyPersonalInfo.html")
    public String modifyPersonalInfo(HttpSession session, HttpServletRequest request){
        Account loginAccount = (Account)session.getAttribute("loginAccount");
        if (loginAccount != null && loginAccount.getAccountCode() != null){// 已登录
            loginAccount.setFooProperties(fooProperties);
            switch (loginAccount.getAccountType().intValue()) {
                case 1:// 管理员
                    Admin admin = adminService.selectByAdminCode(loginAccount);
                    admin.setAccountType(1L);
                    request.setAttribute("account",admin);
                    return "PersonalInfoManage/modifyAdminPersonalInfo";
                case 2:// 教师
                    Teacher teacher = teacherService.selectByTeacherCode(loginAccount);
                    teacher.setAccountType(2L);
                    request.setAttribute("account",teacher);
                    return "PersonalInfoManage/modifyTeacherPersonalInfo";
                case 3:// 学生
                    Student student = studentService.selectByStudentCode(loginAccount);
                    student.setAccountType(3L);
                    request.setAttribute("account",student);
                    return "PersonalInfoManage/modifyStudentPersonalInfo";
                default:// 其他
                    break;
            }
        }
        return "redirect:" + "/myIndex.html?actionFlag=false";
    }

    @RequestMapping("/modifyPersonalInfo.do")
    public String doModifyPersonalInfo( Admin admin, Teacher teacher, Student student, Long accountType,
                                       HttpServletResponse response, HttpSession session) throws IOException {
        PrintWriter writer = response.getWriter();
        boolean actionValue = true;
        if (admin == null
                || teacher == null
                || student == null
                || admin.getAccountCode() == null
                || admin.getAccountName() == null
                || admin.getAccountName().trim().length() == 0
                || teacher.getAccountCode() == null
                || teacher.getAccountName() == null
                || teacher.getAccountName().trim().length() == 0
                || student.getAccountCode() == null
                || student.getAccountName() == null
                || student.getAccountName().trim().length() == 0){
            actionValue = false;
        }
        if (!actionValue){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        Account loginAccount = (Account) session.getAttribute("loginAccount");
        if (loginAccount != null && loginAccount.getAccountCode() != null) {// 已登录
            int affectedRow = 0;
            switch (accountType.intValue()) {
                case 1:// 管理员
                    admin.setUpdatePerson(loginAccount.getAccountCode().intValue());
                    affectedRow = adminService.modifyPersonalInfo(admin);
                    break;
                case 2:// 教师
                    teacher.setUpdatePerson(loginAccount.getAccountCode());
                    teacher.setUpdatePersonType(loginAccount.getAccountType());
                    affectedRow = teacherService.modifyPersonalInfo(teacher);
                    break;
                case 3:// 学生
                    student.setUpdatePerson(loginAccount.getAccountCode());
                    student.setUpdatePersonType(loginAccount.getAccountType());
                    affectedRow = studentService.modifyPersonalInfo(student);
                    break;
                default:// 其他
                    affectedRow = 0;
                    break;
            }
            if (affectedRow != 1) {
                // 异步请求的响应：修改失败
                writer.write("{\"actionFlag\":false}");
            } else {
                common.refreshSessionAccount(admin,teacher,student,accountType,session);
                // 异步请求的响应：修改成功
                writer.write("{\"actionFlag\":true}");
            }
        }
        return null;
    }

    @RequestMapping("/modifyPassword.html")
    public String modifyPassword(){
        return "PersonalInfoManage/modifyPassword";
    }

    @RequestMapping("/modifyPassword.do")
    public String doModifyPassword(Account account, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();
        boolean actionValue = true;
        if (account == null
                || account.getAccountCode() == null
                || account.getOldAccountPassword() == null
                || account.getOldAccountPassword().trim().length() == 0
                || account.getAccountPassword() == null
                || account.getAccountPassword().trim().length() == 0
                || account.getAccountType() == null){
            actionValue = false;
        }
        if (!actionValue){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = 0;
        // 设置属性文件，包含密钥
        account.setFooProperties(fooProperties);
        switch (account.getAccountType().intValue()) {
            case 1:// 管理员
                affectedRow = adminService.modifyPassword(account);
                break;
            case 2:// 教师
                affectedRow = teacherService.modifyPassword(account);
                break;
            case 3:// 学生
                affectedRow = studentService.modifyPassword(account);
                break;
            default:// 其他
                affectedRow = 0;
                break;
        }
        if ( affectedRow != 1){
            // 异步请求的响应：重新设置密码失败
            writer.write("{\"actionFlag\":false}");
        }else {
            // 异步请求的响应：重新设置密码成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
