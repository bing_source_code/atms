package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.PrivilegeMenu;
import com.xhu.atms.service.AccountPrivilegesService;
import com.xhu.atms.service.AdminService;
import com.xhu.atms.service.StudentService;
import com.xhu.atms.service.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by bingge on 2019/3/4.
 */
@Controller
public class LoginController {
    @Resource(name = "adminService")
    private AdminService adminService;

    @Resource(name = "teacherService")
    private TeacherService teacherService;

    @Resource(name = "studentService")
    private StudentService studentService;

    @Resource(name = "accountPrivilegesService")
    private AccountPrivilegesService accountPrivilegesService;

    @Resource
    private FooProperties fooProperties;

    @RequestMapping("/login.html")
    public String login(HttpSession session) {
        session.invalidate();
        return "login";
    }

    @RequestMapping("/myIndex.html")
    public String myIndex() {
        return "myIndex";
    }

    @RequestMapping("/login.do")
    public String doLogin(Account account, HttpSession session) {
        boolean actionValue = true;
        if (account == null
                || account.getAccountCode() == null
                || account.getAccountPassword() == null
                || account.getAccountPassword().trim().length() == 0
                || account.getAccountType() == null) {
            actionValue = false;
        }
        if (!actionValue){
            return "redirect:" + "/login.html?actionValue=false";
        }

        // 登录账号
        Account loginAccount = null;
        // 设置属性文件，包含密钥
        account.setFooProperties(fooProperties);
        switch (account.getAccountType().intValue()) {
            case 1:// 管理员
                loginAccount = adminService.selectByCode(account);
                break;
            case 2:// 教师
                loginAccount = teacherService.selectByCode(account);
                break;
            case 3:// 学生
                loginAccount = studentService.selectByCode(account);
                break;
            default:// 其他
                loginAccount = null;
                break;
        }
        if (loginAccount != null) {
            // 查询此账号有没有登录的权限
            int i = accountPrivilegesService.selectLoginPrivilege(loginAccount.getAccountId());
            if (i != 0) {// 有登录权限
                // 查询此账号的所有权限
                List<PrivilegeMenu> privilegeMenus = accountPrivilegesService.selectAccountPrivileges(loginAccount.getAccountId());
                loginAccount.setAccountType(account.getAccountType());
                session.setAttribute("loginAccount", loginAccount);
                session.setAttribute("privilegeMenus", privilegeMenus);
                return "redirect:" + "/myIndex.html";
            }
        }
        return "redirect:" + "/login.html?actionFlag=false";
    }
}
