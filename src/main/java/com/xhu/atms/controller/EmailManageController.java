package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.*;
import com.xhu.atms.service.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/4/25.
 */
@Controller
@RequestMapping("/EmailManage")
public class EmailManageController {
    @Resource
    private Common common;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private EmailSendService emailSendService;

    @Resource
    private EmailAcceptService emailAcceptService;

    @Resource
    private NextCodeService nextCodeService;

    @Resource
    private TeacherService teacherService;

    @Resource
    private StudentService studentService;

    @RequestMapping("/manageEmailSend.html")
    public String manageEmailSend(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/manageEmailSend";
    }

    @RequestMapping("/manageEmailAccept.html")
    public String manageEmailAccept(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/manageEmailAccept";
    }

    @RequestMapping("/mailDraft.html")
    public String mailDraft(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/mailDraft";
    }

    @RequestMapping("/mailSend.html")
    public String mailSend(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/mailSend";
    }

    @RequestMapping("/mailSendDelete.html")
    public String mailSendDelete(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/mailSendDelete";
    }

    @RequestMapping("/mailNotRead.html")
    public String mailNotRead(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/mailNotRead";
    }

    @RequestMapping("/mailRead.html")
    public String mailRead(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/mailRead";
    }

    @RequestMapping("/mailAcceptDelete.html")
    public String mailAcceptDelete(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "EmailManage/mailAcceptDelete";
    }

    @RequestMapping("/selectPersonType.do")
    public String doSelectPersonType(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "person_type", "email_send");
    }

    @RequestMapping("/selectPerson.do")
    public String doSelectPerson(Teacher teacher,Student student,Long personType, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 用户列表
        List users = null;
        // 保留的属性名称数组
        String fields[] = null;
        switch (personType.intValue()) {
            case 1:// 教师
                teacher.setFooProperties(fooProperties);
                users = teacherService.selectAccount(teacher);
                fields = new String[]{"users","code","msg","count","data","accountId",
                        "accountName", "accountCode","accountDesc","teacherMobilePhoneNum",
                        "teacherEmail", "teacherSchool", "teacherAcademy","teacherMajor",
                        "uploadDate", "uploadUrl", "imgName", "statusCd","statusName",
                        "statusDate", "createPerson", "createDate", "updatePerson",
                        "updatePersonType","updatePersonTypeName", "updateDate"};
                break;
            case 2:// 学生
                student.setFooProperties(fooProperties);
                users = studentService.selectAccount(student);
                fields = new String[]{"users","code","msg","count","data","accountId",
                        "accountName", "accountCode","accountDesc","studentSex",
                        "studentMobilePhoneNum", "studentEmail","studentSchool",
                        "studentAcademy", "studentMajor", "studentClass","uploadDate",
                        "uploadUrl","imgName","statusCd", "statusName", "statusDate",
                        "createPerson", "createPersonType","createPersonTypeName",
                        "createDate", "updatePerson","updatePersonType","updatePersonTypeName",
                        "updateDate"};
                break;
            default:// 其他
                users = null;
                fields = null;
                break;
        }
        // accountMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> accountMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        accountMap.put("code",code);
        accountMap.put("msg",msg);

        // 将所有用户放入accountMap中
        accountMap.put("users", users);

        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,accountMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/selectEmailSendStatusCd.do")
    public String doSelectEmailSendStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "email_send");
    }

    @RequestMapping("/selectEmailAcceptStatusCd.do")
    public String doSelectEmailAcceptStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "email_accept");
    }
    
    @RequestMapping("/selectEmailSend.do")
    public String doSelectEmailSend(EmailSend emailSend,Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果emailSendDesc=""，说明前台没有输入值，将emailSendDesc置为空
        if (emailSend.getEmailSendDesc() != null && emailSend.getEmailSendDesc().trim().length() == 0){
            emailSend.setEmailSendDesc(null);
        }
        // 如果emailSendSubject=""，说明前台没有输入值，将emailSendSubject置为空
        if (emailSend.getEmailSubject() != null && emailSend.getEmailSubject().trim().length() == 0){
            emailSend.setEmailSubject(null);
        }
        // 如果emailSendBody=""，说明前台没有输入值，将emailSendBody置为空
        if (emailSend.getEmailBody() != null && emailSend.getEmailBody().trim().length() == 0){
            emailSend.setEmailBody(null);
        }
        // 如果emailSendAttachment=""，说明前台没有输入值，将emailSendAttachment置为空
        if (emailSend.getEmailAttachment() != null && emailSend.getEmailAttachment().trim().length() == 0){
            emailSend.setEmailAttachment(null);
        }

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = emailSendService.countEmailSend(emailSend);// 带条件的分页查询的总行数
        respMap.put("code",code);
        respMap.put("msg",msg);
        respMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        emailSend.setPager(pager);
        List<EmailSend> emailSends = emailSendService.selectEmailSend(emailSend);

        // 将此页的内容放入respMap中
        respMap.put("data", emailSends);
        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg","count","data","emailSendId",
                "emailSendCode","emailSendDesc","toPerson","toPersonType","toPersonTypeName",
                "fromPerson","fromPersonType","fromPersonTypeName","emailSubject", "emailBody",
                "emailAttachment", "uploadDate","uploadUrl","statusCd","statusName","statusDate",
                "createPerson","createPersonType","createPersonTypeName","createDate",
                "updatePerson","updatePersonType","updatePersonTypeName","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/selectEmailAccept.do")
    public String doSelectEmailAccept(EmailAccept emailAccept, Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果emailAcceptDesc=""，说明前台没有输入值，将emailAcceptDesc置为空
        if (emailAccept.getEmailAcceptDesc() != null && emailAccept.getEmailAcceptDesc().trim().length() == 0){
            emailAccept.setEmailAcceptDesc(null);
        }
        // 如果emailAcceptSubject=""，说明前台没有输入值，将emailAcceptSubject置为空
        if (emailAccept.getEmailSubject() != null && emailAccept.getEmailSubject().trim().length() == 0){
            emailAccept.setEmailSubject(null);
        }
        // 如果emailAcceptBody=""，说明前台没有输入值，将emailAcceptBody置为空
        if (emailAccept.getEmailBody() != null && emailAccept.getEmailBody().trim().length() == 0){
            emailAccept.setEmailBody(null);
        }
        // 如果emailAcceptAttachment=""，说明前台没有输入值，将emailAcceptAttachment置为空
        if (emailAccept.getEmailAttachment() != null && emailAccept.getEmailAttachment().trim().length() == 0){
            emailAccept.setEmailAttachment(null);
        }

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = emailAcceptService.countEmailAccept(emailAccept);// 带条件的分页查询的总行数
        respMap.put("code",code);
        respMap.put("msg",msg);
        respMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        emailAccept.setPager(pager);
        List<EmailAccept> emailAccepts = emailAcceptService.selectEmailAccept(emailAccept);

        // 将此页的内容放入respMap中
        respMap.put("data", emailAccepts);
        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg","count","data","emailAcceptId",
                "emailAcceptCode","emailAcceptDesc","toPerson","toPersonType","toPersonTypeName",
                "fromPerson","fromPersonType","fromPersonTypeName","emailSubject", "emailBody",
                "emailAttachment", "uploadDate","uploadUrl", "statusCd","statusName","statusDate",
                "createPerson","createPersonType","createPersonTypeName","createDate",
                "updatePerson","updatePersonType","updatePersonTypeName","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addEmailSend.do")
    public String doAddEmailSend(EmailSend emailSend, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (emailSend == null
                || emailSend.getToPerson() == null
                || emailSend.getToPersonType() == null
                || emailSend.getFromPerson() == null
                || emailSend.getFromPersonType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 设置发送邮件的编号
        long emailSendCode = nextCodeService.selectNextCode("email_send");
        emailSend.setEmailSendCode(emailSendCode);
        emailSend.setUploadUrl("/upload/email/" + emailSendCode);
        int affectedRow = emailSendService.addEmailSend(emailSend);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加发送邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            boolean addEmailAcceptFlag = true;// 默认添加发送邮件成功
            // 判断是否是发送邮件
            if(emailSend.getStatusCd() == 2){
                // 将发送邮件转换为接收邮件
                EmailAccept emailAccept = changeSendToAccept(emailSend);
                // 将接收邮件添加到数据库中
                addEmailAcceptFlag = addEmailAccept(emailAccept);
            }
            if (addEmailAcceptFlag){
                // 异步请求的响应：添加接收邮件成功，添加发送邮件才成功
                writer.write("{\"actionFlag\":true}");
            }else {
                // 异步请求的响应：由于添加接收邮件失败，因此添加发送邮件也跟着失败
                writer.write("{\"actionFlag\":false}");
            }
        }
        return null;
    }

    @RequestMapping("/addEditEmailSend.do")
    public String doAddEditEmailSend(Long oldEmailSendCode,EmailSend newEmailSend, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (newEmailSend == null
                || oldEmailSendCode == null
                || newEmailSend.getToPerson() == null
                || newEmailSend.getToPersonType() == null
                || newEmailSend.getFromPerson() == null
                || newEmailSend.getFromPersonType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 新邮件编号
        long newEmailSendCode = nextCodeService.selectNextCode("email_send");
        // 获取原邮件
        EmailSend EmailSend1 = new EmailSend();
        EmailSend1.setEmailSendCode(oldEmailSendCode);
        // 原邮件
        EmailSend oldEmailSend = emailSendService.selectEmailSendByCode(EmailSend1);
        // 原邮件的附件名字
        String attachmentName = oldEmailSend.getEmailAttachment();
        // 将原邮件的附件复制新邮件目录
        boolean copyAttachmentFlag = copyAttachment(oldEmailSendCode, newEmailSendCode, attachmentName);
        if (copyAttachmentFlag) {// 如果附件复制成功，再将新邮件信息存入数据库中
            // 设置发送邮件的编号
            newEmailSend.setEmailSendCode(newEmailSendCode);
            newEmailSend.setUploadUrl("/upload/email/" + newEmailSendCode);
            int affectedRow = emailSendService.addEmailSend(newEmailSend);// 受影响的行数
            if (affectedRow != 1) {
                // 异步请求的响应：添加发送邮件失败
                writer.write("{\"actionFlag\":false}");
            } else {
                boolean addEmailAcceptFlag = true;// 默认添加发送邮件成功
                // 判断是否是发送邮件
                if(newEmailSend.getStatusCd() == 2){
                    // 将发送邮件转换为接收邮件
                    EmailAccept emailAccept = changeSendToAccept(newEmailSend);
                    // 将接收邮件添加到数据库中
                    addEmailAcceptFlag = addEmailAccept(emailAccept);
                }
                if (addEmailAcceptFlag){
                    // 异步请求的响应：添加接收邮件成功，添加发送邮件才成功
                    writer.write("{\"actionFlag\":true}");
                }else {
                    // 异步请求的响应：由于添加接收邮件失败，因此添加发送邮件也跟着失败
                    writer.write("{\"actionFlag\":false}");
                }
            }
        }
        return null;
    }

    /**
     * 将发送邮件对象转换为接收邮件对象
     * @param emailSend：需要转换的发送邮件对象
     * @return EmailAccept：转换成功的接收邮件对象
     */
    private EmailAccept changeSendToAccept(EmailSend emailSend){
        EmailAccept emailAccept = new EmailAccept();
        emailAccept.setEmailAcceptCode(emailSend.getEmailSendCode());
        emailAccept.setEmailAcceptDesc(emailSend.getEmailSendDesc());
        emailAccept.setToPerson(emailSend.getToPerson());
        emailAccept.setToPersonType(emailSend.getToPersonType());
        emailAccept.setFromPerson(emailSend.getFromPerson());
        emailAccept.setFromPersonType(emailSend.getFromPersonType());
        emailAccept.setEmailSubject(emailSend.getEmailSubject());
        emailAccept.setEmailBody(emailSend.getEmailBody());
        emailAccept.setEmailAttachment(emailSend.getEmailAttachment());
        emailAccept.setUploadUrl(emailSend.getUploadUrl());
        emailAccept.setStatusCd(1L);// 接收邮件默认是未读
        emailAccept.setCreatePerson(emailSend.getCreatePerson());
        emailAccept.setCreatePersonType(emailSend.getCreatePersonType());
        return emailAccept;
    }

    /**
     * 添加接收邮件
     * @param emailAccept：接收邮件对象
     * @return boolean：是否添加成功，true 成功，false 失败
     */
    private boolean addEmailAccept(EmailAccept emailAccept){
        int addAcceptRow = emailAcceptService.addEmailAccept(emailAccept);
        if(addAcceptRow != 1){
            return false;
        }else {
            return true;
        }
    }

    /**
     * 复制邮件附件，如果是编辑以前的邮件，将原邮件的附件复制到现邮件下
     * @param oldEmailSendCode：原邮件编号
     * @param newEmailSendCode：现邮件编号
     * @param attachmentName：附件名字
     * @return boolean：复制是否成功的标志，true 成功，false 失败
     * @throws IOException
     */
    private boolean copyAttachment(Long oldEmailSendCode,Long newEmailSendCode,String attachmentName) throws IOException {
        String oldRealPath = fooProperties.getUploadEmailAttachmentDir() + "/" + oldEmailSendCode;
        String newRealPath = fooProperties.getUploadEmailAttachmentDir() + "/" + newEmailSendCode;
        // 复制到的新目录
        File newDir = new File(newRealPath);
        if (!newDir.exists()) {
            newDir.mkdir();
        }

        InputStream is = null;// 输入流
        OutputStream os = null;// 输出流
        try {
            File oldFile = new File(oldRealPath + File.separator + attachmentName);
            File newFile = new File(newRealPath + File.separator + attachmentName);
            is = new FileInputStream(oldFile);
            os = new FileOutputStream(newFile);
            int readLines;
            byte[] bytes = new byte[1024];

            while ((readLines = is.read(bytes)) != -1){
                os.write(bytes,0,readLines);
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
            return false;
        } finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
        }
        return true;
    }

    @RequestMapping("/uploadAddEmailAttachment.do")
    public String doUploadAddEmailAttachment( HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        long emailSendCode = nextCodeService.selectNextCode("email_send");
        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg"};
        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        // 上传文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadEmailAttachmentDir() + "/" + emailSendCode;
        // 上传目录
        File uploadDir = new File(realPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // 上传的文件
        Part uploadFilePart = request.getPart("file");
        // 上传的文件名
        String uploadFileName = common.getFileName(uploadFilePart);
        // 读写文件，将上传文件读入字节数组，然后写入到服务器上指定的文件
        respMap = common.readWriteFile(realPath, uploadFileName, uploadFilePart);
        // 将所有发往客户端的信息转化为json数组，并发往客户端
        common.writeRespJsonToClient(fields,respMap,writer);
        return null;
    }

    @RequestMapping("/uploadModifyEmailAttachment.do")
    public String doUploadModifyEmailAttachment( Long emailSendCode, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg"};
        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        if (emailSendCode == null) {
            respMap.put("code",-1);
            respMap.put("msg","emailSendCode is null");
            common.writeRespJsonToClient(fields,respMap,writer);
            return null;
        }
        // 上传文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadEmailAttachmentDir() + "/" + emailSendCode;
        // 上传目录
        File uploadDir = new File(realPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // 上传的文件
        Part uploadFilePart = request.getPart("file");
        // 上传的文件名
        String uploadFileName = common.getFileName(uploadFilePart);
        // 读写文件，将上传文件读入字节数组，然后写入到服务器上指定的文件
        respMap = common.readWriteFile(realPath, uploadFileName, uploadFilePart);
        // 将所有发往客户端的信息转化为json数组，并发往客户端
        common.writeRespJsonToClient(fields,respMap,writer);
        return null;
    }

    @RequestMapping("/downloadEmailSendAttachment.do")
    public String doDownloadEmailSendAttachment(Long emailSendCode, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码
        response.setCharacterEncoding("utf-8");

        if (emailSendCode == null){
            return null;
        }

        EmailSend emailSend = new EmailSend();
        emailSend.setEmailSendCode(emailSendCode);
        EmailSend downloadEmailSend = emailSendService.selectEmailSendByCode(emailSend);
        String downloadEmailAttachmentName = downloadEmailSend.getEmailAttachment();
        String downloadFileName;

        // 获取用户客户端信息
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
            // 针对IE或者以IE为内核的浏览器：
            downloadFileName = URLEncoder.encode(downloadEmailAttachmentName, "UTF-8");
        } else {
            // 非IE浏览器的处理：
            downloadFileName = new String(downloadEmailAttachmentName.getBytes("UTF-8"), "ISO-8859-1");
        }

        // 下载文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadEmailAttachmentDir() + "/" + emailSendCode;
        // 下载文件
        File downLoadFile = new File(realPath + File.separator + downloadEmailAttachmentName);
        // 下载文件长度
        long fileLength = downLoadFile.length();
        // 设置响应
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment; filename=" + downloadFileName);
        response.setHeader("Content-Length", String.valueOf(fileLength));

        // 获取输入流
        InputStream is = new FileInputStream(downLoadFile);
        // 获取输出流
        ServletOutputStream os = response.getOutputStream();

        int readLines = 0;
        byte[] bytes = new byte[1024];
        while ((readLines = is.read(bytes)) != -1){
            os.write(bytes,0,readLines);
        }
        os.flush();

        if (is != null) {
            is.close();
        }
        if (os != null) {
            os.close();
        }
        return null;
    }

    @RequestMapping("/downloadEmailAcceptAttachment.do")
    public String doDownloadEmailAcceptAttachment(Long emailAcceptCode, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        // 设置请求的字符编码
        request.setCharacterEncoding("utf-8");
        // 设置响应的字符编码
        response.setCharacterEncoding("utf-8");

        if (emailAcceptCode == null){
            return null;
        }

        EmailAccept emailAccept = new EmailAccept();
        emailAccept.setEmailAcceptCode(emailAcceptCode);
        EmailAccept downloadEmailAccept = emailAcceptService.selectEmailAcceptByCode(emailAccept);
        String downloadEmailAttachmentName = downloadEmailAccept.getEmailAttachment();
        String downloadFileName;

        // 获取用户客户端信息
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
            // 针对IE或者以IE为内核的浏览器：
            downloadFileName = URLEncoder.encode(downloadEmailAttachmentName, "UTF-8");
        } else {
            // 非IE浏览器的处理：
            downloadFileName = new String(downloadEmailAttachmentName.getBytes("UTF-8"), "ISO-8859-1");
        }

        // 下载文件在服务器的真实存放路径
        String realPath = fooProperties.getUploadEmailAttachmentDir() + "/" + emailAcceptCode;
        // 下载文件
        File downLoadFile = new File(realPath + File.separator + downloadEmailAttachmentName);
        // 下载文件长度
        long fileLength = downLoadFile.length();
        // 设置响应
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment; filename=" + downloadFileName);
        response.setHeader("Content-Length", String.valueOf(fileLength));

        // 获取输入流
        InputStream is = new FileInputStream(downLoadFile);
        // 获取输出流
        ServletOutputStream os = response.getOutputStream();

        int readLines = 0;
        byte[] bytes = new byte[1024];
        while ((readLines = is.read(bytes)) != -1){
            os.write(bytes,0,readLines);
        }
        os.flush();

        if (is != null) {
            is.close();
        }
        if (os != null) {
            os.close();
        }
        return null;
    }

    @RequestMapping("/modifyEmailSend.do")
    public String doModifyEmailSend(EmailSend emailSend, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (emailSend == null
                || emailSend.getEmailSendCode() == null
                || emailSend.getUpdatePerson() == null
                || emailSend.getUpdatePersonType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailSendService.modifyEmailSend(emailSend);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改发送邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            boolean addEmailAcceptFlag = true;// 默认添加发送邮件成功
            // 判断是否是发送邮件
            if(emailSend.getStatusCd() == 2){
                // 重新从数据库查询此邮件编号对应的邮件
                EmailSend emailSend1 = emailSendService.selectEmailSendByCode(emailSend);
                // 将发送邮件转换为接收邮件
                EmailAccept emailAccept = changeSendToAccept(emailSend1);
                // 将接收邮件添加到数据库中
                addEmailAcceptFlag = addEmailAccept(emailAccept);
            }
            if (addEmailAcceptFlag){
                // 异步请求的响应：添加接收邮件成功，添加发送邮件才成功
                writer.write("{\"actionFlag\":true}");
            }else {
                // 异步请求的响应：由于添加接收邮件失败，因此添加发送邮件也跟着失败
                writer.write("{\"actionFlag\":false}");
            }
        }
        return null;
    }

    @RequestMapping("/modifyEmailAccept.do")
    public String doModifyEmailAccept(EmailAccept emailAccept, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (emailAccept == null
                || emailAccept.getEmailAcceptCode() == null
                || emailAccept.getUpdatePerson() == null
                || emailAccept.getUpdatePersonType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailAcceptService.modifyEmailAccept(emailAccept);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改接收邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改接收邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyEmailAcceptsStatus.do")
    public String doModifyEmailAcceptsStatus(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为发送邮件列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<EmailAccept> emailAccepts = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            EmailAccept emailAccept = (EmailAccept)JSONObject.toBean(jsonObject, EmailAccept.class);
            emailAccepts.add(emailAccept);
        }

        boolean actionValue = true;
        if (emailAccepts.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (EmailAccept emailAccept : emailAccepts) {
            // 如果发送邮件编号为空，请求值错误
            if (emailAccept.getEmailAcceptCode() == null
                    || emailAccept.getStatusCd() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailAcceptService.modifyEmailAcceptsStatus(emailAccepts);// 受影响的行数
        if (affectedRow != emailAccepts.size()) {
            // 异步请求的响应：修改接收邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改接收邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyEmailSendsStatus.do")
    public String doModifyEmailSendsStatus(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为发送邮件列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<EmailSend> emailSends = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            EmailSend emailSend = (EmailSend)JSONObject.toBean(jsonObject, EmailSend.class);
            emailSends.add(emailSend);
        }

        boolean actionValue = true;
        if (emailSends.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (EmailSend emailSend : emailSends) {
            // 如果发送邮件编号为空，请求值错误
            if (emailSend.getEmailSendCode() == null
                    || emailSend.getStatusCd() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailSendService.modifyEmailSendsStatus(emailSends);// 受影响的行数
        if (affectedRow != emailSends.size()) {
            // 异步请求的响应：修改接收邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改接收邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteEmailSend.do")
    public String doDeleteEmailSend(EmailSend emailSend, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (emailSend == null
                || emailSend.getEmailSendCode() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailSendService.deleteEmailSend(emailSend);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除发送邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除发送邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteEmailAccept.do")
    public String doDeleteEmailAccept(EmailAccept emailAccept, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (emailAccept == null
                || emailAccept.getEmailAcceptCode() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailAcceptService.deleteEmailAccept(emailAccept);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除接收邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除接收邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteEmailSends.do")
    public String doDeleteEmailSends(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为发送邮件列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<EmailSend> emailSends = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            EmailSend emailSend = (EmailSend)JSONObject.toBean(jsonObject, EmailSend.class);
            emailSends.add(emailSend);
        }

        boolean actionValue = true;
        if (emailSends.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (EmailSend emailSend : emailSends) {
            // 如果发送邮件编号为空，请求值错误
            if (emailSend.getEmailSendCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailSendService.deleteEmailSends(emailSends);// 受影响的行数
        if (affectedRow != emailSends.size()) {
            // 异步请求的响应：删除发送邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除发送邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteEmailAccepts.do")
    public String doDeleteEmailAccepts(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为发送邮件列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<EmailAccept> emailAccepts = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            EmailAccept emailAccept = (EmailAccept)JSONObject.toBean(jsonObject, EmailAccept.class);
            emailAccepts.add(emailAccept);
        }

        boolean actionValue = true;
        if (emailAccepts.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (EmailAccept emailAccept : emailAccepts) {
            // 如果发送邮件编号为空，请求值错误
            if (emailAccept.getEmailAcceptCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = emailAcceptService.deleteEmailAccepts(emailAccepts);// 受影响的行数
        if (affectedRow != emailAccepts.size()) {
            // 异步请求的响应：删除接收邮件失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除接收邮件成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
