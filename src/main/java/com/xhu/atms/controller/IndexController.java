package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by bingge on 2019/3/3.
 */
@Controller
public class IndexController {
    @Resource
    private FooProperties fooProperties;

    @RequestMapping("/index.html")
    public String index(){
        return "index";
    }

    @RequestMapping("/about.html")
    public String about(HttpServletRequest request){
        request.setAttribute("version",fooProperties.getVersion());
        return "about";
    }

    @RequestMapping("/userServiceItem.html")
    public String userServiceItem(){
        return "userServiceItem";
    }

    @RequestMapping("/skin-config.html")
    public String skinConfig(){
        return "skin-config.html";
    }
}
