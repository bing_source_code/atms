package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.*;
import com.xhu.atms.service.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@MultipartConfig
@RequestMapping("/UserManage")
public class UserManageController {
    @Resource
    private Common common;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private NextCodeService nextCodeService;

    @Resource
    private AdminService adminService;

    @Resource
    private TeacherService teacherService;

    @Resource
    private StudentService studentService;

    @RequestMapping("/manageTeacher.html")
    public String manageTeacher(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "UserManage/manageTeacher";
    }

    @RequestMapping("/manageStudent.html")
    public String manageStudent(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "UserManage/manageStudent";
    }

    @RequestMapping("/selectUserStatusCd.do")
    public String doSelectAccountStatusCd(Long accountType,HttpServletResponse response) throws IOException {
        switch (accountType.intValue()) {
            case 1:// 管理员
                return common.doSelectMapCode(response, "status_cd", "admin");
            case 2:// 教师
                return common.doSelectMapCode(response, "status_cd", "teacher");
            case 3:// 学生
                return common.doSelectMapCode(response, "status_cd", "student");
            default:// 其他
                return null;
        }
    }

    @RequestMapping("/selectUser.do")
    public String doSelectAccount(Teacher teacher,Student student,Long accountType,
                                  Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null
                || accountType == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 带条件的分页查询的总行数
        int count = 0;
        // 带条件的分页查询的一页内容
        List users = null;
        // 保留的属性名称数组
        String fields[] = null;
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        switch (accountType.intValue()) {
            case 2:// 教师
                // 如果accountName=""，说明前台没有输入值，将accountName置为空
                if (teacher.getAccountName() != null && teacher.getAccountName().trim().length() == 0){
                    teacher.setAccountName(null);
                }
                // 如果accountDesc=""，说明前台没有输入值，将accountDesc置为空
                if (teacher.getAccountDesc() != null && teacher.getAccountDesc().trim().length() == 0){
                    teacher.setAccountDesc(null);
                }
                teacher.setPager(pager);
                teacher.setFooProperties(fooProperties);
                count = teacherService.countAccount(teacher);
                users = teacherService.selectAccount(teacher);
                fields = new String[]{"code","msg","count","data","accountId",
                        "accountName", "accountCode","accountDesc","teacherMobilePhoneNum",
                        "teacherEmail", "teacherSchool", "teacherAcademy","teacherMajor",
                        "uploadDate", "uploadUrl", "imgName", "statusCd","statusName",
                        "statusDate", "createPerson", "createDate", "updatePerson",
                        "updatePersonType","updatePersonTypeName", "updateDate"};
                break;
            case 3:// 学生
                // 如果accountName=""，说明前台没有输入值，将accountName置为空
                if (student.getAccountName() != null && student.getAccountName().trim().length() == 0){
                    student.setAccountName(null);
                }
                // 如果accountDesc=""，说明前台没有输入值，将accountDesc置为空
                if (student.getAccountDesc() != null && student.getAccountDesc().trim().length() == 0){
                    student.setAccountDesc(null);
                }
                student.setPager(pager);
                student.setFooProperties(fooProperties);
                count = studentService.countAccount(student);
                users = studentService.selectAccount(student);
                fields = new String[]{"code","msg","count","data","accountId",
                        "accountName", "accountCode","accountDesc","studentSex",
                        "studentMobilePhoneNum", "studentEmail","studentSchool",
                        "studentAcademy", "studentMajor", "studentClass","uploadDate",
                        "uploadUrl","imgName","statusCd", "statusName", "statusDate",
                        "createPerson", "createPersonType","createPersonTypeName",
                        "createDate", "updatePerson","updatePersonType","updatePersonTypeName",
                        "updateDate"};
                break;
            default:// 其他
                count = 0;
                users = null;
                break;
        }
        // accountMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> accountMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        accountMap.put("code",code);
        accountMap.put("msg",msg);
        accountMap.put("count",count);

        // 将此页的内容放入accountMap中
        accountMap.put("data", users);

        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,accountMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addUser.do")
    public String doAddUser(Teacher teacher,Student student,Long accountType,
                            HttpServletResponse response, HttpSession session) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (teacher == null
                || student == null
                || teacher.getAccountName() == null
                || teacher.getAccountName().trim().length() == 0
                || student.getAccountName() == null
                || student.getAccountName().trim().length() == 0){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 设置用户的编号
        long accountCode = 0;
        int affectedRow = 0;// 受影响的行数
        switch (accountType.intValue()) {
            case 2:// 教师
                accountCode = nextCodeService.selectNextCode("teacher");
                teacher.setAccountCode(accountCode);
                teacher.setFooProperties(fooProperties);
                affectedRow = teacherService.addAccount(teacher);
                break;
            case 3:// 学生
                accountCode = nextCodeService.selectNextCode("student");
                student.setAccountCode(accountCode);
                student.setFooProperties(fooProperties);
                affectedRow = studentService.addStudent(student);
                break;
            default:// 其他
                affectedRow = 0;
                break;
        }

        if (affectedRow != 1) {
            // 异步请求的响应：添加用户失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加用户成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/uploadUserImg.do")
    public String doUploadUserImg(Account account, HttpServletRequest request, HttpSession session,
                                  HttpServletResponse response) throws IOException, ServletException, URISyntaxException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 保留的属性名称数组
        String fields[] = new String[]{"code","msg"};
        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        if (account.getAccountCode() == null
                || account.getAccountType() == null) {
            respMap.put("code",-1);
            respMap.put("msg","accountCode or accountType is null");
            common.writeRespJsonToClient(fields,respMap,writer);
            return null;
        }

        // 上传文件在服务器的真实存放路径
        String realPath;
        String rootPath = this.getClass().getClassLoader().getResource("/").toURI().getPath();
        switch (account.getAccountType().intValue()) {
            case 1:// 管理员
                realPath = rootPath + "/static/images/admin/" + account.getAccountCode();
                break;
            case 2:// 教师
                realPath = rootPath + "/static/images/teacher/" + account.getAccountCode();
                break;
            case 3:// 学生
                realPath = rootPath + "/static/images/student" + account.getAccountCode();
                break;
            default:// 其他
                realPath = "";
                break;
        }

        // 上传目录
        File uploadDir = new File(realPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        // 上传的文件
        Part uploadFilePart = request.getPart("file");
        // 上传的文件名
        String uploadFileName = common.getFileName(uploadFilePart);
        // 读写文件，将上传文件读入字节数组，然后写入到服务器上指定的文件
        respMap = common.readWriteFile(realPath, uploadFileName, uploadFilePart);
        // 将所有发往客户端的信息转化为json数组，并发往客户端
        common.writeRespJsonToClient(fields,respMap,writer);
        return null;
    }

    @RequestMapping("/modifyUser.do")
    public String doModifyUser(Admin admin, Teacher teacher,Student student,Long accountType,
                               HttpSession session,HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (teacher == null
                || student == null
                || admin == null
                || admin.getAccountCode() == null
                || teacher.getAccountCode() == null
                || student.getAccountCode() == null){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        Account loginAccount = (Account) session.getAttribute("loginAccount");
        if (loginAccount != null && loginAccount.getAccountCode() != null) {// 已登录
            int affectedRow = 0;// 受影响的行数
            switch (accountType.intValue()) {
                case 1:// 管理员
                    admin.setUpdatePerson(loginAccount.getAccountCode().intValue());
                    String adminImgName = admin.getImgName();
                    if (adminImgName != null && adminImgName.trim().length() != 0) {
                        admin.setUploadUrl("/images/admin/" + admin.getAccountCode());
                    }
                    affectedRow = adminService.modifyPersonalInfo(admin);
                    break;
                case 2:// 教师
                    teacher.setUpdatePerson(loginAccount.getAccountCode());
                    teacher.setUpdatePersonType(loginAccount.getAccountType());
                    String teacherImgName = teacher.getImgName();
                    if (teacherImgName != null && teacherImgName.trim().length() != 0) {
                        teacher.setUploadUrl("/images/teacher/" + teacher.getAccountCode());
                    }
                    affectedRow = teacherService.modifyPersonalInfo(teacher);
                    break;
                case 3:// 学生
                    student.setUpdatePerson(loginAccount.getAccountCode());
                    student.setUpdatePersonType(loginAccount.getAccountType());
                    String studentImgName = student.getImgName();
                    if (studentImgName != null && studentImgName.trim().length() != 0) {
                        student.setUploadUrl("/images/student/" + student.getAccountCode());
                    }
                    affectedRow = studentService.modifyPersonalInfo(student);
                    break;
                default:// 其他
                    affectedRow = 0;
                    break;
            }
            if (affectedRow != 1) {
                // 异步请求的响应：修改用户失败
                writer.write("{\"actionFlag\":false}");
            } else {
                common.refreshSessionAccount(admin,teacher,student,accountType,session);
                // 异步请求的响应：修改用户成功
                writer.write("{\"actionFlag\":true}");
            }
        }
        return null;
    }

    @RequestMapping("/deleteUser.do")
    public String doDeleteUser(Teacher teacher,Student student,Long accountType,
                               HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (teacher == null
                || student == null
                || teacher.getAccountCode() == null
                || student.getAccountCode() == null){
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = 0;// 受影响的行数
        switch (accountType.intValue()) {
            case 2:// 教师
                affectedRow = teacherService.deleteAccount(teacher);
                break;
            case 3:// 学生
                affectedRow = studentService.deleteAccount(student);
                break;
            default:// 其他
                affectedRow = 0;
                break;
        }
        if (affectedRow != 1) {
            // 异步请求的响应：删除用户失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除用户成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteUsers.do")
    public String doDeleteAccounts(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为用户列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List users = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Long accountType = jsonObject.getLong("accountType");
            switch (accountType.intValue()) {
                case 2:// 教师
                    Teacher teacher = (Teacher) JSONObject.toBean(jsonObject, Teacher.class);
                    users.add(teacher);
                    break;
                case 3:// 学生
                    Student student = (Student) JSONObject.toBean(jsonObject, Student.class);
                    users.add(student);
                    break;
                default:// 其他
                    break;
            }
        }

        boolean actionValue = true;
        if (users.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (Object user : users) {
            Account account = (Account) user;
            // 如果账号编号为空，请求值错误
            if (account.getAccountCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRowSum = 0;// 受影响的总行数
        boolean actionFlag = false;// 是否删除失败的标志
        for (int i = 0;i < users.size();i++) {
            int affectedRow = 0;// 受影响的行数
            Account account = (Account) users.get(affectedRowSum);
            switch (account.getAccountType().intValue()) {
                case 2:// 教师
                    Teacher teacher = (Teacher) users.get(affectedRowSum);
                    affectedRow += teacherService.deleteAccount(teacher);
                    if (affectedRow == 1){
                        affectedRowSum += affectedRow;
                    }else {
                        actionFlag = true;
                    }
                    break;
                case 3:// 学生
                    Student student = (Student) users.get(affectedRowSum);
                    affectedRow = studentService.deleteAccount(student);
                    if (affectedRow == 1){
                        affectedRowSum += affectedRow;
                    }else {
                        actionFlag = true;
                    }
                    break;
                default:// 其他
                    actionFlag = true;
                    break;
            }
            if (actionFlag){// 如果删除失败，跳出循环
                break;
            }
        }

        if (affectedRowSum != users.size()) {
            // 异步请求的响应：删除用户失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除用户成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
