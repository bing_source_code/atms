package com.xhu.atms.controller;

import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.Page;
import com.xhu.atms.entity.Privilege;
import com.xhu.atms.service.NextCodeService;
import com.xhu.atms.service.PrivilegeService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@RequestMapping("/PrivManage")
public class PrivManageController {
    @Resource
    private Common common;

    @Resource
    private PrivilegeService privilegeService;

    @Resource
    private NextCodeService nextCodeService;

    @RequestMapping("/managePriv.html")
    public String managePriv(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "PrivManage/managePriv";
    }

    @RequestMapping("/selectPrivStatusCd.do")
    public String doSelectPrivStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response, "status_cd", "privilege");
    }

    @RequestMapping("/selectPriv.do")
    public String doSelectPriv(Privilege privilege, Integer page, Integer limit, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果privilegeName=""，说明前台没有输入值，将privilegeName置为空
        if (privilege.getPrivilegeName() != null && privilege.getPrivilegeName().trim().length() == 0){
            privilege.setPrivilegeName(null);
        }
        // 如果privilegeDesc=""，说明前台没有输入值，将privilegeDesc置为空
        if (privilege.getPrivilegeDesc() != null && privilege.getPrivilegeDesc().trim().length() == 0){
            privilege.setPrivilegeDesc(null);
        }

        // privMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = privilegeService.countPriv(privilege);// 带条件的分页查询的总行数
        privMap.put("code",code);
        privMap.put("msg",msg);
        privMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        privilege.setPager(pager);
        List<Privilege> privileges = privilegeService.selectPriv(privilege);

        // 将此页的内容放入privMap中
        privMap.put("data", privileges);
        // 保留的属性名称数组
        String fields[] = new String[]{"code", "msg", "count", "data",
                "privilegeId","privilegeName", "privilegeCode", "privilegeDesc",
                "url","statusCd" ,"statusName","statusDate", "createPerson",
                "createDate","updatePerson", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addPriv.do")
    public String doAddPriv(Privilege privilege, HttpServletResponse response, HttpSession session) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (privilege == null
                || privilege.getPrivilegeName() == null
                || privilege.getPrivilegeName().trim().length() == 0
                || privilege.getUrl() == null
                || privilege.getUrl().trim().length() == 0) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        // 设置权限的编号
        long privilegeCode = nextCodeService.selectNextCode("privilege");
        privilege.setPrivilegeCode(privilegeCode);
        int affectedRow = privilegeService.addPrivilege(privilege);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加权限失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加权限成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyPriv.do")
    public String doModifyPriv(Privilege privilege, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (privilege == null
                || privilege.getPrivilegeCode() == null
                || privilege.getPrivilegeName() == null
                || privilege.getPrivilegeName().trim().length() == 0
                || privilege.getUrl() == null
                || privilege.getUrl().trim().length() == 0) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = privilegeService.modifyPrivilege(privilege);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改权限失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改权限成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deletePriv.do")
    public String doDeletePriv(Privilege privilege, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (privilege == null
                || privilege.getPrivilegeCode() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = privilegeService.deletePrivilege(privilege);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除权限失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除权限成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deletePrivs.do")
    public String doDeletePrivs(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为权限列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<Privilege> privileges = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Privilege privilege = (Privilege)JSONObject.toBean(jsonObject, Privilege.class);
            privileges.add(privilege);
        }

        boolean actionValue = true;
        if (privileges.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (Privilege privilege : privileges) {
            // 如果权限编号为空，请求值错误
            if (privilege.getPrivilegeCode() == null){
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = privilegeService.deletePrivileges(privileges);// 受影响的行数
        if (affectedRow != privileges.size()) {
            // 异步请求的响应：删除权限失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除权限成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
