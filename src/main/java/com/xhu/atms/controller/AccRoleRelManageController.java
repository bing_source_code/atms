package com.xhu.atms.controller;

import com.xhu.atms.config.FooProperties;
import com.xhu.atms.config.IgnoreFieldProcessorImpl;
import com.xhu.atms.config.Common;
import com.xhu.atms.entity.*;
import com.xhu.atms.service.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/3/26.
 */
@Controller
@RequestMapping("/AccRoleRelManage")
public class AccRoleRelManageController {
    @Resource
    private FooProperties fooProperties;

    @Resource
    private Common common;

    @Resource
    private AccRoleRelService accRoleRelService;

    @Resource
    private AdminService adminService;

    @Resource
    private TeacherService teacherService;

    @Resource
    private StudentService studentService;

    @RequestMapping("/manageAccRoleRel.html")
    public String manageAccRoleRel(Long privCode, HttpServletRequest request){
        if (privCode != null){
            request.setAttribute("privCode",privCode);
        }
        return "AccRoleRelManage/manageAccRoleRel";
    }

    @RequestMapping("/selectAccount.do")
    public String doSelectAccount(Admin admin, Teacher teacher, Student student, Long accType,
                                  HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (accType == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 带条件的分页查询的一页内容
        List users = null;
        // 保留的属性名称数组
        String fields[] = null;
        switch (accType.intValue()) {
            case 1:// 管理员
                admin.setFooProperties(fooProperties);
                users = adminService.selectAccount(admin);
                fields = new String[]{"users","accountId","accountName", "accountCode",
                        "accountDesc","uploadDate", "uploadUrl", "imgName","statusCd",
                        "statusName", "statusDate","createPerson", "createDate",
                        "updatePerson", "updateDate"};
                break;
            case 2:// 教师
                teacher.setFooProperties(fooProperties);
                users = teacherService.selectAccount(teacher);
                fields = new String[]{"users","code","msg","count","data","accountId",
                        "accountName", "accountCode","accountDesc","teacherMobilePhoneNum",
                        "teacherEmail", "teacherSchool", "teacherAcademy","teacherMajor",
                        "uploadDate", "uploadUrl", "imgName", "statusCd","statusName",
                        "statusDate", "createPerson", "createDate", "updatePerson",
                        "updatePersonType","updatePersonTypeName", "updateDate"};
                break;
            case 3:// 学生
                student.setFooProperties(fooProperties);
                users = studentService.selectAccount(student);
                fields = new String[]{"users","code","msg","count","data","accountId",
                        "accountName", "accountCode","accountDesc","studentSex",
                        "studentMobilePhoneNum", "studentEmail","studentSchool",
                        "studentAcademy", "studentMajor", "studentClass","uploadDate",
                        "uploadUrl","imgName","statusCd", "statusName", "statusDate",
                        "createPerson", "createPersonType","createPersonTypeName",
                        "createDate", "updatePerson","updatePersonType","updatePersonTypeName",
                        "updateDate"};
                break;
            default:// 其他
                users = null;
                break;
        }
        // accountMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> accountMap  = new HashMap<>();
        // 将此页的内容放入accountMap中
        accountMap.put("users", users);

        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,accountMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/selectRole.do")
    public String doSelectRole(Role role, HttpServletResponse response) throws IOException {
        return common.doSelectRole(role,response);
    }

    @RequestMapping("/selectAccRoleRelStatusCd.do")
    public String doSelectAccRoleRelStatusCd(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response,"status_cd","acc_role_rel");
    }

    @RequestMapping("/selectAccRoleRelAccType.do")
    public String doSelectAccRoleRelAccType(HttpServletResponse response) throws IOException {
        return common.doSelectMapCode(response,"acc_type","acc_role_rel");
    }

    @RequestMapping("/selectAccRoleRel.do")
    public String doSelectAccRoleRel(AccRoleRel accRoleRel, Integer page, Integer limit, HttpServletResponse response)
            throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        if (page == null
                || limit == null){
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":true}");
            return null;
        }

        // 如果accRoleRelDesc=""，说明前台没有输入值，将accRoleRelDesc置为空
        if (accRoleRel.getRelDesc() != null && accRoleRel.getRelDesc().trim().length() == 0){
            accRoleRel.setRelDesc(null);
        }

        // accRoleRelMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> accRoleRelMap  = new HashMap<>();
        // 设置layui table组件默认规定的数据格式，code、msg和count
        int code = 0;// 状态码：成功
        String msg = "success";// 消息：成功
        int count = accRoleRelService.countAccRoleRel(accRoleRel);// 带条件的分页查询的总行数
        accRoleRelMap.put("code",code);
        accRoleRelMap.put("msg",msg);
        accRoleRelMap.put("count",count);

        // 查询分页的当前页的内容
        // 设置分页查询的参数，当前页号和一页显示的数量
        Page pager = new Page();
        pager.setPage(page);
        pager.setLimit(limit);
        accRoleRel.setPager(pager);
        List<AccRoleRel> accRoleRels = accRoleRelService.selectAccRoleRel(accRoleRel);

        // 将此页的内容放入accRoleRelMap中
        accRoleRelMap.put("data", accRoleRels);
        // 保留的属性名称数组
        String fields[] = new String[]{"code", "msg", "count", "data",
                "accId","accName","accType","accTypeName","roleId","roleName","relDesc",
                "statusCd","statusName","statusDate","createPerson","createPersonType",
                "createPersonTypeName","createDate","updatePerson", "updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,accRoleRelMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    @RequestMapping("/addAccRoleRel.do")
    public String doAddAccRoleRel(AccRoleRel accRoleRel, HttpServletResponse response, HttpSession session) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (accRoleRel == null
                || accRoleRel.getAccId() == null
                || accRoleRel.getRoleId() == null
                || accRoleRel.getAccType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }
        
        int affectedRow = accRoleRelService.addAccRoleRel(accRoleRel);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：添加账号角色关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：添加账号角色关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/modifyAccRoleRel.do")
    public String doModifyAccRoleRel(AccRoleRel accRoleRel, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (accRoleRel == null
                || accRoleRel.getAccId() == null
                || accRoleRel.getRoleId() == null
                || accRoleRel.getAccType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = accRoleRelService.modifyAccRoleRel(accRoleRel);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：修改账号角色关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：修改账号角色关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteAccRoleRel.do")
    public String doDeleteAccRoleRel(AccRoleRel accRoleRel, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        boolean actionValue = true;
        if (accRoleRel == null
                || accRoleRel.getAccId() == null
                || accRoleRel.getRoleId() == null
                || accRoleRel.getAccType() == null) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRow = accRoleRelService.deleteAccRoleRel(accRoleRel);// 受影响的行数
        if (affectedRow != 1) {
            // 异步请求的响应：删除账号角色关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除账号角色关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }

    @RequestMapping("/deleteAccRoleRels.do")
    public String doDeleteAccRoleRels(String data, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // 将前台传过来的json数组转换为账号角色关系列表
        JSONArray jsonArray= JSONArray.fromObject(data);
        List<AccRoleRel> accRoleRels = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            AccRoleRel accRoleRel = (AccRoleRel)JSONObject.toBean(jsonObject, AccRoleRel.class);
            accRoleRels.add(accRoleRel);
        }

        boolean actionValue = true;
        if (accRoleRels.isEmpty()) {
            actionValue = false;
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        for (AccRoleRel accRoleRel : accRoleRels) {
            // 如果角色编号或者权限编号为空，请求值错误
            if (accRoleRel.getAccId() == null
                    || accRoleRel.getRoleId() == null
                    || accRoleRel.getAccType() == null) {
                actionValue = false;
                break;
            }
        }
        if (!actionValue) {
            // 异步请求的响应：值错误
            writer.write("{\"actionValue\":false}");
            return null;
        }

        int affectedRowSum = 0;// 受影响的行数
        for (AccRoleRel accRoleRel : accRoleRels) {
            int affectedRow = accRoleRelService.deleteAccRoleRel(accRoleRel);
            if (affectedRow == 1){
                affectedRowSum += affectedRow;
            }else {
                break;
            }
        }
        if (affectedRowSum != accRoleRels.size()) {
            // 异步请求的响应：删除账号角色关系失败
            writer.write("{\"actionFlag\":false}");
        } else {
            // 异步请求的响应：删除账号角色关系成功
            writer.write("{\"actionFlag\":true}");
        }
        return null;
    }
}
