package com.xhu.atms.dao;

import org.apache.ibatis.annotations.Param;

/**
 * Created by bingge on 2019/3/30.
 *下一个编号的Mapper类
 */
public interface NextCodeMapper {
    /**
     * 查询编号自动增加的表的下一个编号
     * @param tableName：表名
     * @return long：下一个编号
     */
    long selectNextCode(@Param("tableName")String tableName);
}
