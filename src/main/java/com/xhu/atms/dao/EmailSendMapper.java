package com.xhu.atms.dao;

import com.xhu.atms.entity.EmailSend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by bingge on 2019/3/26.
 * 发送邮件Mapper类
 */
public interface EmailSendMapper {
    /**
     * 统计带条件的分页查询总行数
     * @param emailSend：发送邮件对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countEmailSend(EmailSend emailSend);

    /**
     * 通过条件查询发送邮件
     * @param emailSend：发送邮件对象，包含条件
     * @return List<EmailSend> ：发送邮件对象列表
     */
    List<EmailSend> selectEmailSend(EmailSend emailSend);

    /**
     * 通过发送邮件编号查询发送邮件
     * @param emailSend：发送邮件对象，包含发送邮件编号
     * @return List<EmailSend> ：发送邮件对象
     */
    EmailSend selectEmailSendByCode(EmailSend emailSend);

    /**
     * 添加发送邮件
     * @param emailSend：需要添加的发送邮件对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addEmailSend(EmailSend emailSend);

    /**
     * 修改发送邮件
     * @param emailSend：发送邮件对象，包含发送邮件编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyEmailSend(EmailSend emailSend);

    /**
     * 修改发送邮件状态
     * @param emailSends：发送邮件对象列表，包含发送邮件编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int modifyEmailSendsStatus(@Param("emailSends") List<EmailSend> emailSends);

    /**
     * 删除发送邮件，不会真的删除，只是将状态改为删除
     * @param emailSend：发送邮件对象，包含发送邮件编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteEmailSend(EmailSend emailSend);

    /**
     * 删除发送邮件，不会真的删除，只是将状态改为删除
     * @param emailSends：发送邮件对象列表，包含发送邮件编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteEmailSends(@Param("emailSends") List<EmailSend> emailSends);
}
