package com.xhu.atms.dao;

import com.xhu.atms.entity.AccountPrivileges;
import com.xhu.atms.entity.PrivilegeMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by bingge on 2019/3/16.
 * 账号权限Mapper类
 */
public interface AccountPrivilegesMapper {
    /**
     * 通过账号编号查询一个账号是否有登录权限
     * @param accId：账号编号
     * @return int：0 没有，1 有
     */
    int selectLoginPrivilege(@Param("acc_id")long accId);

    /**
     * 通过账号编号查询一个账号除登录权限以外的所有权限
     * @param accId：账号编号
     * @return List<PrivilegeMenu>：权限列表
     */
    List<PrivilegeMenu> selectAccountPrivileges(@Param("acc_id")long accId);
}
