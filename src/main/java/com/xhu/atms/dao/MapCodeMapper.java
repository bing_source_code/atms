package com.xhu.atms.dao;

import com.xhu.atms.entity.MapCode;

import java.util.List;

/**
 * Created by bingge on 2019/4/1.
 * MapCode 码值表Mapper类
 */
public interface MapCodeMapper {
    /**
     * 通过列名和表名查询map_code表
     * @param mapCode：码值对象，包含列名和表名
     * @return List<MapCode>：码值对象列表
     */
    List<MapCode> selectMapCode(MapCode mapCode);
}
