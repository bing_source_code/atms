package com.xhu.atms.dao;

import com.xhu.atms.entity.Course;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by bingge on 2019/3/26.
 * 课程Mapper类
 */
public interface CourseMapper {
    /**
     * 统计带条件的分页查询总行数
     * @param course：课程对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countCourse(Course course);

    /**
     * 通过条件查询课程
     * @param course：课程对象，包含条件
     * @return List<Course> ：课程对象列表
     */
    List<Course> selectCourse(Course course);

    /**
     * 添加课程
     * @param course：需要添加的课程对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addCourse(Course course);

    /**
     * 修改课程
     * @param course：课程对象，包含课程编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyCourse(Course course);

    /**
     * 删除课程，不会真的删除，只是将状态改为失效
     * @param course：课程对象，包含课程编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteCourse(Course course);

    /**
     * 删除课程，不会真的删除，只是将状态改为失效
     * @param courses：课程对象列表，包含课程编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteCourses(@Param("courses") List<Course> courses);
}
