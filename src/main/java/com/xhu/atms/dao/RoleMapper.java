package com.xhu.atms.dao;

import com.xhu.atms.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by bingge on 2019/3/26.
 * 角色Mapper类
 */
public interface RoleMapper {
    /**
     * 统计带条件的分页查询总行数
     * @param role：角色对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countRole(Role role);

    /**
     * 通过条件查询角色
     * @param role：角色对象，包含条件
     * @return List<Role> ：角色对象列表
     */
    List<Role> selectRole(Role role);

    /**
     * 添加角色
     * @param role：需要添加的角色对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addRole(Role role);

    /**
     * 修改角色
     * @param role：角色对象，包含角色编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyRole(Role role);

    /**
     * 删除角色，不会真的删除，只是将状态改为失效
     * @param role：角色对象，包含角色编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteRole(Role role);

    /**
     * 删除角色，不会真的删除，只是将状态改为失效
     * @param roles：角色对象列表，包含角色编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteRoles(@Param("roles") List<Role> roles);
}
