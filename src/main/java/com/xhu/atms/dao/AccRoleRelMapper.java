package com.xhu.atms.dao;

import com.xhu.atms.entity.AccRoleRel;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * Created by bingge on 2019/3/16.
 * 账号角色关系Mapper类
 */
public interface AccRoleRelMapper {
    /**
     * 统计带条件的分页查询总行数
     * @param accRoleRel：账号角色关系对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countAccRoleRel(AccRoleRel accRoleRel);

    /**
     * 通过条件查询账号角色关系
     * @param accRoleRel：账号角色关系对象，包含条件
     * @return List<AccRoleRel> ：账号角色关系对象列表
     */
    List<AccRoleRel> selectAccRoleRel(AccRoleRel accRoleRel);

    /**
     * 添加账号角色关系
     * @param accRoleRel：需要添加的账号角色关系对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addAccRoleRel(AccRoleRel accRoleRel);

    /**
     * 修改账号角色关系
     * @param accRoleRel：账号角色关系对象，包含角色编号和权限编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyAccRoleRel(AccRoleRel accRoleRel);

    /**
     * 删除账号角色关系，不会真的删除，只是将状态改为失效
     * @param accRoleRel：账号角色关系对象，包含角色编号和权限编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteAccRoleRel(AccRoleRel accRoleRel);
}
