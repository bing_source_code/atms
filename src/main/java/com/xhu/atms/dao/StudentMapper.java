package com.xhu.atms.dao;

import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Student;
import com.xhu.atms.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by bingge on 2019/3/14.
 * 学生Mapper类
 */
public interface StudentMapper {
    /**
     * 通过学生编号和密码查询学生
     * @param account：账号对象，包含学生编号和密码
     * @return Account：账号对象
     */
    Account selectByCode(Account account);

    /**
     * 添加学生
     * @param student：需要添加的学生对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addStudent(Student student);

    /**
     * 验证学生信息是否正确
     * @param account：账号对象，包含要验证的学生姓名、编号、描述
     * @return int：1 成功，其他 失败
     */
    int validateInfo(Account account);

    /**
     * 重新设置学生密码
     * @param account：账号对象，包含学生编号和新密码
     * @return int：1 成功，其他 失败
     */
    int resetPassword(Account account);

    /**
     * 通过学生编号查询学生
     * @param account：账号对象，包含学生编号
     * @return Student：学生对象
     */
    Student selectByStudentCode(Account account);

    /**
     * 通过学生编号修改学生的个人信息
     * @param student：学生对象，包含学生编号及要修改的信息
     * @return int：受影响的行数
     */
    int modifyPersonalInfo(Student student);

    /**
     * 通过学生编号和密码修改学生密码
     * @param account：账号对象，包含学生编号和密码
     * @return int：受影响的行数
     */
    int modifyPassword(Account account);

    /**
     * 统计带条件的分页查询总行数
     * @param student：学生对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countAccount(Student student);

    /**
     * 通过条件查询账号
     * @param student：学生对象，包含条件
     * @return List<Student> ：账号对象列表
     */
    List<Student> selectAccount(Student student);

    /**
     * 删除账号，不会真的删除，只是将状态改为失效
     * @param student：学生对象，包含账号编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteAccount(Student student);

    /**
     * 删除账号，不会真的删除，只是将状态改为失效
     * @param students：学生对象列表，包含账号编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteAccounts(@Param("students") List<Student> students);
}
