package com.xhu.atms.entity;

import java.util.Date;

/**
 * Created by bingge on 2019/3/11.
 * 管理员实体类
 */
public class Admin extends Account {
    private Long statusCd;
    private Date statusDate;
    private Integer createPerson;
    private Date createDate;
    private Integer updatePerson;
    private Date updateDate;

    public Admin() {
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "Admin{" +
                super.toString() +
                "statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updateDate=" + updateDate +
                '}';
    }
}
