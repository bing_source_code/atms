package com.xhu.atms.entity;

import java.io.Serializable;

/**
 * Created by bingge on 2019/3/16.
 * 账号权限类
 */
public class AccountPrivileges implements Serializable {
    private Long accId;
    private Long roleId;
    private Integer accType;
    private Long privilegeId;
    private String privilegeName;
    private Long privilegeCode;
    private String privilegeDesc;
    private String url;

    public AccountPrivileges() {
    }

    public Long getAccId() {
        return accId;
    }

    public void setAccId(Long accId) {
        this.accId = accId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Integer getAccType() {
        return accType;
    }

    public void setAccType(Integer accType) {
        this.accType = accType;
    }

    public Long getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Long privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public Long getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(Long privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "AccountPrivileges{" +
                "accId=" + accId +
                ", roleId=" + roleId +
                ", accType=" + accType +
                ", privilegeId=" + privilegeId +
                ", privilegeName='" + privilegeName + '\'' +
                ", privilegeCode=" + privilegeCode +
                ", privilegeDesc='" + privilegeDesc + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
