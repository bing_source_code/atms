package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/3/16.
 * 权限实体类
 */
public class Privilege implements Serializable {
    private Long privilegeId;
    private String privilegeName;
    private Long privilegeCode;
    private String privilegeDesc;
    private String url;
    private Long statusCd;
    private Date statusDate;
    private Integer createPerson;
    private Date createDate;
    private Integer updatePerson;
    private Date updateDate;
    private String statusName;// 状态码对应的状态名字
    private Page pager;// 分页查询的页面对象

    public Privilege() {
    }

    public Long getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Long privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public Long getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(Long privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "Privilege{" +
                "privilegeId=" + privilegeId +
                ", privilegeName='" + privilegeName + '\'' +
                ", privilegeCode=" + privilegeCode +
                ", privilegeDesc='" + privilegeDesc + '\'' +
                ", url='" + url + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updateDate=" + updateDate +
                ", statusName='" + statusName + '\'' +
                ", pager=" + pager +
                '}';
    }
}
