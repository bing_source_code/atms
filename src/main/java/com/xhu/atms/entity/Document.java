package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/4/14.
 * 文档资料实体类
 */
public class Document implements Serializable {
    private Long docId;
    private String docName;
    private Long docCode;
    private String docDesc;
    private Long courseCode;
    private Long teacherCode;
    private Long docType;
    private Date uploadDate;
    private String uploadUrl;
    private String attachmentName;// 附件名字
    private Long statusCd;
    private Date statusDate;
    private Long createPerson;
    private Long createPersonType;
    private Date createDate;
    private Long updatePerson;
    private Long updatePersonType;
    private Date updateDate;
    private String courseName;// 课程名字
    private String teacherName;// 教师名字
    private String docTypeName;// 文档类型名字
    private String statusName;// 状态名字
    private String createPersonTypeName;// 创建人类型名字
    private String updatePersonTypeName;// 更新人类型名字
    private Page pager;// 分页查询的页面对象
    private boolean sortByStatusDate;// 是否根据状态时间排序的标志

    public Document() {
    }

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public Long getDocCode() {
        return docCode;
    }

    public void setDocCode(Long docCode) {
        this.docCode = docCode;
    }

    public String getDocDesc() {
        return docDesc;
    }

    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }

    public Long getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(Long courseCode) {
        this.courseCode = courseCode;
    }

    public Long getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(Long teacherCode) {
        this.teacherCode = teacherCode;
    }

    public Long getDocType() {
        return docType;
    }

    public void setDocType(Long docType) {
        this.docType = docType;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Long getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Long createPerson) {
        this.createPerson = createPerson;
    }

    public Long getCreatePersonType() {
        return createPersonType;
    }

    public void setCreatePersonType(Long createPersonType) {
        this.createPersonType = createPersonType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Long updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Long getUpdatePersonType() {
        return updatePersonType;
    }

    public void setUpdatePersonType(Long updatePersonType) {
        this.updatePersonType = updatePersonType;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getDocTypeName() {
        return docTypeName;
    }

    public void setDocTypeName(String docTypeName) {
        this.docTypeName = docTypeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCreatePersonTypeName() {
        return createPersonTypeName;
    }

    public void setCreatePersonTypeName(String createPersonTypeName) {
        this.createPersonTypeName = createPersonTypeName;
    }

    public String getUpdatePersonTypeName() {
        return updatePersonTypeName;
    }

    public void setUpdatePersonTypeName(String updatePersonTypeName) {
        this.updatePersonTypeName = updatePersonTypeName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    public boolean isSortByStatusDate() {
        return sortByStatusDate;
    }

    public void setSortByStatusDate(boolean sortByStatusDate) {
        this.sortByStatusDate = sortByStatusDate;
    }

    @Override
    public String toString() {
        return "Document{" +
                "docId=" + docId +
                ", docName='" + docName + '\'' +
                ", docCode=" + docCode +
                ", docDesc='" + docDesc + '\'' +
                ", courseCode=" + courseCode +
                ", teacherCode=" + teacherCode +
                ", docType=" + docType +
                ", uploadDate=" + uploadDate +
                ", uploadUrl='" + uploadUrl + '\'' +
                ", attachmentName='" + attachmentName + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createPersonType=" + createPersonType +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updatePersonType=" + updatePersonType +
                ", updateDate=" + updateDate +
                ", courseName='" + courseName + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", docTypeName='" + docTypeName + '\'' +
                ", statusName='" + statusName + '\'' +
                ", createPersonTypeName='" + createPersonTypeName + '\'' +
                ", updatePersonTypeName='" + updatePersonTypeName + '\'' +
                ", pager=" + pager +
                ", sortByStatusDate=" + sortByStatusDate +
                '}';
    }
}
