package com.xhu.atms.entity;

import java.util.Date;

/**
 * Created by bingge on 2019/3/12.
 * 教师实体类
 */
public class Teacher extends Account {
    private String teacherMobilePhoneNum;
    private String teacherEmail;
    private String teacherSchool;
    private String teacherAcademy;
    private String teacherMajor;
    private Long statusCd;
    private Date statusDate;
    private Long createPerson;
    private Date createDate;
    private Long updatePerson;
    private Long updatePersonType;
    private Date updateDate;
    private String updatePersonTypeName;// 更新人类型名

    public Teacher() {
    }

    public String getTeacherMobilePhoneNum() {
        return teacherMobilePhoneNum;
    }

    public void setTeacherMobilePhoneNum(String teacherMobilePhoneNum) {
        this.teacherMobilePhoneNum = teacherMobilePhoneNum;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherSchool() {
        return teacherSchool;
    }

    public void setTeacherSchool(String teacherSchool) {
        this.teacherSchool = teacherSchool;
    }

    public String getTeacherAcademy() {
        return teacherAcademy;
    }

    public void setTeacherAcademy(String teacherAcademy) {
        this.teacherAcademy = teacherAcademy;
    }

    public String getTeacherMajor() {
        return teacherMajor;
    }

    public void setTeacherMajor(String teacherMajor) {
        this.teacherMajor = teacherMajor;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Long getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Long createPerson) {
        this.createPerson = createPerson;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Long updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Long getUpdatePersonType() {
        return updatePersonType;
    }

    public void setUpdatePersonType(Long updatePersonType) {
        this.updatePersonType = updatePersonType;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdatePersonTypeName() {
        return updatePersonTypeName;
    }

    public void setUpdatePersonTypeName(String updatePersonTypeName) {
        this.updatePersonTypeName = updatePersonTypeName;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherMobilePhoneNum='" + teacherMobilePhoneNum + '\'' +
                ", teacherEmail='" + teacherEmail + '\'' +
                ", teacherSchool='" + teacherSchool + '\'' +
                ", teacherAcademy='" + teacherAcademy + '\'' +
                ", teacherMajor='" + teacherMajor + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updatePersonType=" + updatePersonType +
                ", updateDate=" + updateDate +
                ", updatePersonTypeName='" + updatePersonTypeName + '\'' +
                '}';
    }
}
