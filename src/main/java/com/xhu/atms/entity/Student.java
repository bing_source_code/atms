package com.xhu.atms.entity;

import java.util.Date;

/**
 * Created by bingge on 2019/3/12.
 * 学生实体类
 */
public class Student extends Account{
    private String studentSex;
    private String studentMobilePhoneNum;
    private String studentEmail;
    private String studentSchool;
    private String studentAcademy;
    private String studentMajor;
    private String studentClass;
    private Long statusCd;
    private Date statusDate;
    private Long createPerson;
    private Long createPersonType;
    private Date createDate;
    private Long updatePerson;
    private Long updatePersonType;
    private Date updateDate;
    private String createPersonTypeName;// 创建人类型名字
    private String updatePersonTypeName;// 修改人类型名字

    public Student() {
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex;
    }

    public String getStudentMobilePhoneNum() {
        return studentMobilePhoneNum;
    }

    public void setStudentMobilePhoneNum(String studentMobilePhoneNum) {
        this.studentMobilePhoneNum = studentMobilePhoneNum;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentSchool() {
        return studentSchool;
    }

    public void setStudentSchool(String studentSchool) {
        this.studentSchool = studentSchool;
    }

    public String getStudentAcademy() {
        return studentAcademy;
    }

    public void setStudentAcademy(String studentAcademy) {
        this.studentAcademy = studentAcademy;
    }

    public String getStudentMajor() {
        return studentMajor;
    }

    public void setStudentMajor(String studentMajor) {
        this.studentMajor = studentMajor;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public void setStudentClass(String studentClass) {
        this.studentClass = studentClass;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Long getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Long createPerson) {
        this.createPerson = createPerson;
    }

    public Long getCreatePersonType() {
        return createPersonType;
    }

    public void setCreatePersonType(Long createPersonType) {
        this.createPersonType = createPersonType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Long updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Long getUpdatePersonType() {
        return updatePersonType;
    }

    public void setUpdatePersonType(Long updatePersonType) {
        this.updatePersonType = updatePersonType;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreatePersonTypeName() {
        return createPersonTypeName;
    }

    public void setCreatePersonTypeName(String createPersonTypeName) {
        this.createPersonTypeName = createPersonTypeName;
    }

    public String getUpdatePersonTypeName() {
        return updatePersonTypeName;
    }

    public void setUpdatePersonTypeName(String updatePersonTypeName) {
        this.updatePersonTypeName = updatePersonTypeName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentSex='" + studentSex + '\'' +
                ", studentMobilePhoneNum='" + studentMobilePhoneNum + '\'' +
                ", studentEmail='" + studentEmail + '\'' +
                ", studentSchool='" + studentSchool + '\'' +
                ", studentAcademy='" + studentAcademy + '\'' +
                ", studentMajor='" + studentMajor + '\'' +
                ", studentClass='" + studentClass + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createPersonType=" + createPersonType +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updatePersonType=" + updatePersonType +
                ", updateDate=" + updateDate +
                ", createPersonTypeName='" + createPersonTypeName + '\'' +
                ", updatePersonTypeName='" + updatePersonTypeName + '\'' +
                '}';
    }
}
