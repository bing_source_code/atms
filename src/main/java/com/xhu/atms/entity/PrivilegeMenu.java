package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bingge on 2019/3/16.
 * 权限菜单
 */
public class PrivilegeMenu implements Serializable {
    private String menuType;// 菜单类型
    private List<AccountPrivileges> menuItems;// 菜单项集合

    public PrivilegeMenu() {
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public List<AccountPrivileges> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<AccountPrivileges> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public String toString() {
        return "PrivilegeMenu{" +
                "menuType='" + menuType + '\'' +
                ", menuItems=" + menuItems +
                '}';
    }
}
