package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/4/26.
 * 发送邮件实体类
 */
public class EmailSend implements Serializable {
    private Long emailSendId;
    private Long emailSendCode;
    private String emailSendDesc;
    private Long toPerson;
    private Long toPersonType;
    private Long fromPerson;
    private Long fromPersonType;
    private String emailSubject;
    private String emailBody;
    private String emailAttachment;
    private Date uploadDate;
    private String uploadUrl;
    private Long statusCd;
    private Date statusDate;
    private Long createPerson;
    private Long createPersonType;
    private Date createDate;
    private Long updatePerson;
    private Long updatePersonType;
    private Date updateDate;
    private String toPersonTypeName;// 收件人类型名字
    private String fromPersonTypeName;// 发件人类型名字
    private String statusName;// 状态名字
    private String createPersonTypeName;// 创建人类型名字
    private String updatePersonTypeName;// 更新人类型名字
    private Page pager;// 分页查询的页面对象

    public EmailSend() {
    }

    public Long getEmailSendId() {
        return emailSendId;
    }

    public void setEmailSendId(Long emailSendId) {
        this.emailSendId = emailSendId;
    }

    public Long getEmailSendCode() {
        return emailSendCode;
    }

    public void setEmailSendCode(Long emailSendCode) {
        this.emailSendCode = emailSendCode;
    }

    public String getEmailSendDesc() {
        return emailSendDesc;
    }

    public void setEmailSendDesc(String emailSendDesc) {
        this.emailSendDesc = emailSendDesc;
    }

    public Long getToPerson() {
        return toPerson;
    }

    public void setToPerson(Long toPerson) {
        this.toPerson = toPerson;
    }

    public Long getToPersonType() {
        return toPersonType;
    }

    public void setToPersonType(Long toPersonType) {
        this.toPersonType = toPersonType;
    }

    public Long getFromPerson() {
        return fromPerson;
    }

    public void setFromPerson(Long fromPerson) {
        this.fromPerson = fromPerson;
    }

    public Long getFromPersonType() {
        return fromPersonType;
    }

    public void setFromPersonType(Long fromPersonType) {
        this.fromPersonType = fromPersonType;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailAttachment() {
        return emailAttachment;
    }

    public void setEmailAttachment(String emailAttachment) {
        this.emailAttachment = emailAttachment;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Long getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Long createPerson) {
        this.createPerson = createPerson;
    }

    public Long getCreatePersonType() {
        return createPersonType;
    }

    public void setCreatePersonType(Long createPersonType) {
        this.createPersonType = createPersonType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Long updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Long getUpdatePersonType() {
        return updatePersonType;
    }

    public void setUpdatePersonType(Long updatePersonType) {
        this.updatePersonType = updatePersonType;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getToPersonTypeName() {
        return toPersonTypeName;
    }

    public void setToPersonTypeName(String toPersonTypeName) {
        this.toPersonTypeName = toPersonTypeName;
    }

    public String getFromPersonTypeName() {
        return fromPersonTypeName;
    }

    public void setFromPersonTypeName(String fromPersonTypeName) {
        this.fromPersonTypeName = fromPersonTypeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCreatePersonTypeName() {
        return createPersonTypeName;
    }

    public void setCreatePersonTypeName(String createPersonTypeName) {
        this.createPersonTypeName = createPersonTypeName;
    }

    public String getUpdatePersonTypeName() {
        return updatePersonTypeName;
    }

    public void setUpdatePersonTypeName(String updatePersonTypeName) {
        this.updatePersonTypeName = updatePersonTypeName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "EmailSend{" +
                "emailSendId=" + emailSendId +
                ", emailSendCode=" + emailSendCode +
                ", emailSendDesc='" + emailSendDesc + '\'' +
                ", toPerson=" + toPerson +
                ", toPersonType=" + toPersonType +
                ", fromPerson=" + fromPerson +
                ", fromPersonType=" + fromPersonType +
                ", emailSubject='" + emailSubject + '\'' +
                ", emailBody='" + emailBody + '\'' +
                ", emailAttachment='" + emailAttachment + '\'' +
                ", uploadDate=" + uploadDate +
                ", uploadUrl='" + uploadUrl + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createPersonType=" + createPersonType +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updatePersonType=" + updatePersonType +
                ", updateDate=" + updateDate +
                ", toPersonTypeName='" + toPersonTypeName + '\'' +
                ", fromPersonTypeName='" + fromPersonTypeName + '\'' +
                ", statusName='" + statusName + '\'' +
                ", createPersonTypeName='" + createPersonTypeName + '\'' +
                ", updatePersonTypeName='" + updatePersonTypeName + '\'' +
                ", pager=" + pager +
                '}';
    }
}
