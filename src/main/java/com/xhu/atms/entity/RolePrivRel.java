package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/3/16.
 * 角色权限关系类
 */
public class RolePrivRel implements Serializable {
    private Long roleId;
    private Long privilegeId;
    private String relDesc;
    private Long statusCd;
    private Date statusDate;
    private Integer createPerson;
    private Date createDate;
    private Integer updatePerson;
    private Date updateDate;
    private String roleName;// 角色id对应的角色名字
    private String privilegeName;// 权限id对应的权限名字
    private String statusName;// 状态码对应的状态名字
    private Page pager;// 分页查询的页面对象

    public RolePrivRel() {
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Long privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getRelDesc() {
        return relDesc;
    }

    public void setRelDesc(String relDesc) {
        this.relDesc = relDesc;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "RolePrivRel{" +
                "roleId=" + roleId +
                ", privilegeId=" + privilegeId +
                ", relDesc='" + relDesc + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updateDate=" + updateDate +
                ", roleName='" + roleName + '\'' +
                ", privilegeName='" + privilegeName + '\'' +
                ", statusName='" + statusName + '\'' +
                ", pager=" + pager +
                '}';
    }
}
