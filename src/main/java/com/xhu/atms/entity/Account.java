package com.xhu.atms.entity;

import com.xhu.atms.config.FooProperties;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/3/14.
 * 账号类，账号包含管理员、教师和学生
 */
@Component
public class Account implements Serializable {
    private Long accountId;
    private String accountName;
    private Long accountCode;
    private String accountPassword;
    private String accountDesc;
    private Date uploadDate;// 头像上传时间
    private String uploadUrl;// 头像上传路径
    private String imgName;// 头像名字

    private Long accountType;// 账号类型
    @Resource
    private FooProperties fooProperties;// 配置文件，里面存有密钥
    private String oldAccountPassword;// 原密码

    private String statusName;// 状态码对应的状态名字
    private Page pager;// 分页查询的页面对象

    public Account() {
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(Long accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public String getAccountDesc() {
        return accountDesc;
    }

    public void setAccountDesc(String accountDesc) {
        this.accountDesc = accountDesc;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public Long getAccountType() {
        return accountType;
    }

    public void setAccountType(Long accountType) {
        this.accountType = accountType;
    }

    public FooProperties getFooProperties() {
        return fooProperties;
    }

    public void setFooProperties(FooProperties fooProperties) {
        this.fooProperties = fooProperties;
    }

    public String getOldAccountPassword() {
        return oldAccountPassword;
    }

    public void setOldAccountPassword(String oldAccountPassword) {
        this.oldAccountPassword = oldAccountPassword;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", accountName='" + accountName + '\'' +
                ", accountCode=" + accountCode +
                ", accountPassword='" + accountPassword + '\'' +
                ", accountDesc='" + accountDesc + '\'' +
                ", uploadDate=" + uploadDate +
                ", uploadUrl='" + uploadUrl + '\'' +
                ", imgName='" + imgName + '\'' +
                ", accountType=" + accountType +
                ", fooProperties=" + fooProperties +
                ", oldAccountPassword='" + oldAccountPassword + '\'' +
                ", statusName='" + statusName + '\'' +
                ", pager=" + pager +
                '}';
    }
}
