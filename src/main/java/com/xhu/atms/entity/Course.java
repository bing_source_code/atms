package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/4/14.
 * 课程实体类
 */
public class Course implements Serializable {
    private Long courseId;
    private String courseName;
    private Long courseCode;
    private String courseDesc;
    private String courseSchool;
    private String courseAcademy;
    private String courseMajor;
    private Long statusCd;
    private Date statusDate;
    private Integer createPerson;
    private Date createDate;
    private Integer updatePerson;
    private Date updateDate;
    private String statusName;// 状态码对应的状态名字
    private Page pager;// 分页查询的页面对象

    public Course() {
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Long getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(Long courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseDesc() {
        return courseDesc;
    }

    public void setCourseDesc(String courseDesc) {
        this.courseDesc = courseDesc;
    }

    public String getCourseSchool() {
        return courseSchool;
    }

    public void setCourseSchool(String courseSchool) {
        this.courseSchool = courseSchool;
    }

    public String getCourseAcademy() {
        return courseAcademy;
    }

    public void setCourseAcademy(String courseAcademy) {
        this.courseAcademy = courseAcademy;
    }

    public String getCourseMajor() {
        return courseMajor;
    }

    public void setCourseMajor(String courseMajor) {
        this.courseMajor = courseMajor;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Integer getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Integer createPerson) {
        this.createPerson = createPerson;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", courseCode=" + courseCode +
                ", courseDesc='" + courseDesc + '\'' +
                ", courseSchool='" + courseSchool + '\'' +
                ", courseAcademy='" + courseAcademy + '\'' +
                ", courseMajor='" + courseMajor + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updateDate=" + updateDate +
                ", statusName='" + statusName + '\'' +
                ", pager=" + pager +
                '}';
    }
}
