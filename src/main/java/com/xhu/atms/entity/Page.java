package com.xhu.atms.entity;

import java.io.Serializable;

/**
 * Created by bingge on 2019/3/29.
 * 分页查询的页面实体类
 */
public class Page implements Serializable {
    private Integer count;// 分页查询的总行数
    private Integer page;// 分页查询的当前页号
    private Integer limit;// 分页查询的一页显示的数量
    private Integer beginNo;// 开始序号
    private Integer endNo;// 结束序号

    public Page() {
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getBeginNo() {
        return (page -1)*limit;
    }

    public void setBeginNo(Integer beginNo) {
        this.beginNo = beginNo;
    }

    public Integer getEndNo() {
        return page*limit;
    }

    public void setEndNo(Integer endNo) {
        this.endNo = endNo;
    }

    @Override
    public String toString() {
        return "Page{" +
                "count=" + count +
                ", page=" + page +
                ", limit=" + limit +
                ", beginNo=" + beginNo +
                ", endNo=" + endNo +
                '}';
    }
}
