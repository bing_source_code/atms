package com.xhu.atms.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bingge on 2019/3/16.
 * 账号角色关系类
 */
public class AccRoleRel implements Serializable {
    private Long accId;
    private Long roleId;
    private Long accType;
    private String relDesc;
    private Long statusCd;
    private Date statusDate;
    private Long createPerson;
    private Long createPersonType;
    private Date createDate;
    private Integer updatePerson;
    private Date updateDate;
    private String accName;// 账号名字
    private String roleName;// 角色名字
    private String accTypeName;// 账号类型名字
    private String statusName;// 状态名字
    private String createPersonTypeName;// 创建人类型名字
    private Page pager;// 分页查询的页面对象

    public AccRoleRel() {
    }

    public Long getAccId() {
        return accId;
    }

    public void setAccId(Long accId) {
        this.accId = accId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getAccType() {
        return accType;
    }

    public void setAccType(Long accType) {
        this.accType = accType;
    }

    public String getRelDesc() {
        return relDesc;
    }

    public void setRelDesc(String relDesc) {
        this.relDesc = relDesc;
    }

    public Long getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(Long statusCd) {
        this.statusCd = statusCd;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Long getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(Long createPerson) {
        this.createPerson = createPerson;
    }

    public Long getCreatePersonType() {
        return createPersonType;
    }

    public void setCreatePersonType(Long createPersonType) {
        this.createPersonType = createPersonType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getAccTypeName() {
        return accTypeName;
    }

    public void setAccTypeName(String accTypeName) {
        this.accTypeName = accTypeName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getCreatePersonTypeName() {
        return createPersonTypeName;
    }

    public void setCreatePersonTypeName(String createPersonTypeName) {
        this.createPersonTypeName = createPersonTypeName;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    @Override
    public String toString() {
        return "AccRoleRel{" +
                "accId=" + accId +
                ", roleId=" + roleId +
                ", accType=" + accType +
                ", relDesc='" + relDesc + '\'' +
                ", statusCd=" + statusCd +
                ", statusDate=" + statusDate +
                ", createPerson=" + createPerson +
                ", createPersonType=" + createPersonType +
                ", createDate=" + createDate +
                ", updatePerson=" + updatePerson +
                ", updateDate=" + updateDate +
                ", accName='" + accName + '\'' +
                ", roleName='" + roleName + '\'' +
                ", accTypeName='" + accTypeName + '\'' +
                ", statusName='" + statusName + '\'' +
                ", createPersonTypeName='" + createPersonTypeName + '\'' +
                ", pager=" + pager +
                '}';
    }
}
