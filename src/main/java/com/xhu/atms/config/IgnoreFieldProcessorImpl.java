package com.xhu.atms.config;

import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.PropertyFilter;

/**
 * Created by bingge on 2019/3/29.
 * 对象属性过滤器，并能对对象中date格式字段进行字符串格式转换
 */
public class IgnoreFieldProcessorImpl implements PropertyFilter {
    /**
     * 保留的属性名称数组
     */
    private String[] fields;

    /**
     * 空参构造方法
     * 默认不忽略集合
     */
    public IgnoreFieldProcessorImpl() {
    }

    /**
     * 构造方法
     *
     * @param fields 保留的属性名称数组
     */
    public IgnoreFieldProcessorImpl(String[] fields) {
        this.fields = fields;
    }

    public boolean apply(Object source, String name, Object value) {
        // 保留设定的属性
        if (fields != null && fields.length > 0) {
            if (juge(fields, name)) {
                return false;
            } else {
                return true;

            }
        }
        return false;
    }

    /**
     * 判断属性名称是否在保留的属性名称数组中
     *
     * @param s  保留的属性名称数组
     * @param s2 保留的属性名称
     * @return：在 返回true，不在 返回false
     */
    public boolean juge(String[] s, String s2) {
        boolean b = false;
        for (String sl : s) {
            if (s2.equals(sl)) {
                b = true;
                break;
            }
        }
        return b;
    }

    /**
     * 获取保留的属性
     */
    public String[] getFields() {
        return fields;
    }

    /**
     * 设置保留的属性
     *
     * @param fields
     */
    public void setFields(String[] fields) {
        this.fields = fields;
    }

    /**
     * 保留字段转换json 对象
     *
     * @param configs 保留字段名称
     * @param entity  需要转换实体
     * @return
     */
    public static JSONObject JsonConfig(String[] configs, Object entity) {
        JsonConfig config = new JsonConfig();
        config.setJsonPropertyFilter(new IgnoreFieldProcessorImpl(configs)); // 保留的属性
        config.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor()); // 将对象中的日期进行格式化
        JSONObject fromObject = JSONObject.fromObject(entity, config);
        return fromObject;
    }

    /**
     * 保留字段转换json 数组
     *
     * @param configs 保留字段名称
     * @param entity  需要转换实体
     * @return
     */
    public static JSONArray ArrayJsonConfig(String[] configs, Object entity) {
        JsonConfig config = new JsonConfig();
        config.setJsonPropertyFilter(new IgnoreFieldProcessorImpl(configs)); // 保留的属性
        config.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor()); // 将对象中的日期进行格式化
        JSONArray fromObject = JSONArray.fromObject(entity, config);
        return fromObject;
    }
}
