package com.xhu.atms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by bingge on 2019/3/22.
 * Foo属性类，存放配置文件中的属性
 */
@Component
@ConfigurationProperties(prefix = "foo")
public class FooProperties {
    private String version;
    private String keyt;
    private String uploadDocDir;
    private String uploadEmailAttachmentDir;

    public FooProperties() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getKeyt() {
        return keyt;
    }

    public void setKeyt(String keyt) {
        this.keyt = keyt;
    }

    public String getUploadDocDir() {
        return uploadDocDir;
    }

    public void setUploadDocDir(String uploadDocDir) {
        this.uploadDocDir = uploadDocDir;
    }

    public String getUploadEmailAttachmentDir() {
        return uploadEmailAttachmentDir;
    }

    public void setUploadEmailAttachmentDir(String uploadEmailAttachmentDir) {
        this.uploadEmailAttachmentDir = uploadEmailAttachmentDir;
    }

    @Override
    public String toString() {
        return "FooProperties{" +
                "version='" + version + '\'' +
                ", keyt='" + keyt + '\'' +
                ", uploadDocDir='" + uploadDocDir + '\'' +
                ", uploadEmailAttachmentDir='" + uploadEmailAttachmentDir + '\'' +
                '}';
    }
}
