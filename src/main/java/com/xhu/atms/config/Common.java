package com.xhu.atms.config;

import com.xhu.atms.entity.*;
import com.xhu.atms.service.*;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bingge on 2019/4/14.
 * 公共类，包含一些共用的方法，全局通用。
 */
@Component
public class Common {
    @Resource
    private MapCodeService mapCodeService;

    @Resource
    private RoleService roleService;

    @Resource
    private FooProperties fooProperties;

    @Resource
    private AdminService adminService;

    @Resource
    private TeacherService teacherService;

    @Resource
    private StudentService studentService;

    /**
     * 通过列名和表名查询map_code表
     * @param response：响应对象
     * @param colName：列名
     * @param tableName：表名
     * @return null：异步请求，返回空
     * @throws IOException
     */
    public String doSelectMapCode(HttpServletResponse response, String colName, String tableName)
            throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // privMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> privMap  = new HashMap<>();
        MapCode mapCode = new MapCode();
        mapCode.setColName(colName);
        mapCode.setTableName(tableName);
        List<MapCode> mapCodes = mapCodeService.selectMapCode(mapCode);
        privMap.put("mapCodes",mapCodes);
        // 保留的属性名称数组
        String fields[] = new String[]{"mapCodes","codeId","codeName","code","codeDesc",
                "colName", "tableName","createPerson","createDate","updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,privMap);

        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    /**
     * 查询角色表
     * @param role：角色对象
     * @param response：响应对象
     * @return null：异步请求，返回空
     * @throws IOException
     */
    public String doSelectRole(Role role, HttpServletResponse response) throws IOException {
        // 设置输出的字符编码和内容类型
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();

        // respMap：Map对象，包含所有发往客户端的信息
        Map<String,Object> respMap  = new HashMap<>();
        List<Role> roles = roleService.selectRole(role);
        respMap.put("roles",roles);
        // 保留的属性名称数组
        String fields[] = new String[]{"roles","roleId","roleName","roleCode", "roleDesc",
                "statusCd", "statusName","statusDate","createPerson", "createDate",
                "updatePerson","updateDate"};
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);

        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
        return null;
    }

    /**
     * 从Part中取文件名
     * @param part：Part对象，从请求里面取到的，包含文件名等信息
     * @return 文件名
     */
    public String getFileName(Part part){
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")){
                String filename = content.substring(content.lastIndexOf("=") + 1).
                        trim().replace("\"","");
                return filename;
            }
        }
        return null;
    }

    /**
     * 将所有发往客户端的信息转化为json数组，并发往客户端
     * @param fields：保留的属性名称数组
     * @param respMap：Map对象，包含所有发往客户端的信息
     * @param writer：打印输出器，输出到客户端
     */
    public void writeRespJsonToClient(String fields[], Map<String,Object> respMap, PrintWriter writer){
        // 将所有发往客户端的信息转化为json数组，并且将日期格式化
        JSONArray jsonArray = IgnoreFieldProcessorImpl.ArrayJsonConfig(fields,respMap);
        // 去掉json数组最外面的[]，并输出到客户端
        writer.write(jsonArray.toString().substring(1,jsonArray.toString().length()-1));
    }

    /**
     * 读写文件，将上传文件读入字节数组，然后写入到服务器上指定的文件
     * @param realPath：上传文件在服务器的真实路径
     * @param uploadFileName：上传文件名
     * @param uploadFilePart：Part类对象，包含上传的文件
     * @throws IOException
     */
    public Map<String,Object> readWriteFile(String realPath, String uploadFileName,Part uploadFilePart)
            throws IOException {
        InputStream is = null;// 输入流
        OutputStream os = null;// 输出流
        int code;// 状态码
        String msg;// 消息
        try {
            File file = new File(realPath + File.separator + uploadFileName);
            is = uploadFilePart.getInputStream();
            os = new FileOutputStream(file);
            int readLines;
            byte[] bytes = new byte[1024];

            while ((readLines = is.read(bytes)) != -1){
                os.write(bytes,0,readLines);
            }

            code = 0; // 上传成功
            msg = "success";
        }catch (IOException ioe){
            code = -1;// 上传失败
            msg = "failed";
            ioe.printStackTrace();
        } finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
        }
        Map<String,Object> respMap  = new HashMap<>();
        respMap.put("code",code);
        respMap.put("msg",msg);
        return respMap;
    }

    /**
     * 刷新session里面的登录用户，重新查一次数据库。
     * @param admin：管理员对象，包含管理员编号
     * @param teacher：教师对象，包含教师编号
     * @param student：学生对象，包含学生编号
     * @param accountType：账号类型，1 管理员，2 教师， 3 学生
     * @param session：session对象
     */
    public void refreshSessionAccount(Admin admin, Teacher teacher, Student student,
                              Long accountType, HttpSession session){
        Account newLoginAccount = null;
        // 重新查询登录账号，并写入到session中
        switch (accountType.intValue()) {
            case 1:// 管理员
                admin.setFooProperties(fooProperties);
                newLoginAccount = adminService.selectByAdminCode(admin);
                break;
            case 2:// 教师
                teacher.setFooProperties(fooProperties);
                newLoginAccount = teacherService.selectByTeacherCode(teacher);
                break;
            case 3:// 学生
                student.setFooProperties(fooProperties);
                newLoginAccount = studentService.selectByStudentCode(student);
                break;
            default:// 其他
                newLoginAccount = null;
                break;
        }
        if (newLoginAccount != null) {
            newLoginAccount.setAccountType(accountType);
            session.setAttribute("loginAccount", newLoginAccount);
        }
    }
}
