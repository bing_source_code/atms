package com.xhu.atms.service;

import com.xhu.atms.entity.RolePrivRel;

import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 * 角色权限关系服务类
 */
public interface RolePrivRelService {
    /**
     * 统计带条件的分页查询总数行
     * @param rolePrivRel：角色权限关系对象，包含条件
     * @return int：带条件的分页查询总数行
     */
    int countRolePrivRel(RolePrivRel rolePrivRel);

    /**
     * 通过条件查询角色权限关系
     * @param rolePrivRel：角色权限关系对象，包含条件
     * @return List<RolePrivRel> ：角色权限关系对象列表
     */
    List<RolePrivRel> selectRolePrivRel(RolePrivRel rolePrivRel);

    /**
     * 添加角色权限关系
     * @param rolePrivRel：需要添加的角色权限关系对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addRolePrivRel(RolePrivRel rolePrivRel);

    /**
     * 修改角色权限关系
     * @param rolePrivRel：角色权限关系对象，包含角色权限关系编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyRolePrivRel(RolePrivRel rolePrivRel);

    /**
     * 删除角色权限关系，不会真的删除，只是将状态改为失效
     * @param rolePrivRel：角色权限关系对象，包含角色权限关系编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteRolePrivRel(RolePrivRel rolePrivRel);
}
