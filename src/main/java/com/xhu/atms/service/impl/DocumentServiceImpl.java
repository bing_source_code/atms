package com.xhu.atms.service.impl;

import com.xhu.atms.dao.DocumentMapper;
import com.xhu.atms.entity.Document;
import com.xhu.atms.service.DocumentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 */
@Service("docService")
public class DocumentServiceImpl implements DocumentService {
    @Resource(name = "documentMapper")
    private DocumentMapper documentMapper;

    @Override
    public int countDocument(Document document) {
        return documentMapper.countDocument(document);
    }

    @Override
    public List<Document> selectDocument(Document document) {
        return documentMapper.selectDocument(document);
    }

    @Override
    public Document selectDocumentByCode(Document document) {
        return documentMapper.selectDocumentByCode(document);
    }

    @Override
    public int addDocument(Document document) {
        return documentMapper.addDocument(document);
    }

    @Override
    public int modifyDocument(Document document) {
        return documentMapper.modifyDocument(document);
    }

    @Override
    public int deleteDocument(Document document) {
        return documentMapper.deleteDocument(document);
    }

    @Override
    public int deleteDocuments(List<Document> documents) {
        return documentMapper.deleteDocuments(documents);
    }
}
