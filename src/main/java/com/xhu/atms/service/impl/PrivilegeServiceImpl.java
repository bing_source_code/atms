package com.xhu.atms.service.impl;

import com.xhu.atms.dao.PrivilegeMapper;
import com.xhu.atms.entity.Privilege;
import com.xhu.atms.service.PrivilegeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 */
@Service("privService")
public class PrivilegeServiceImpl implements PrivilegeService {
    @Resource(name = "privilegeMapper")
    private PrivilegeMapper privilegeMapper;

    @Override
    public int countPriv(Privilege privilege) {
        return privilegeMapper.countPriv(privilege);
    }

    @Override
    public List<Privilege> selectPriv(Privilege privilege) {
        return privilegeMapper.selectPriv(privilege);
    }

    @Override
    public int addPrivilege(Privilege privilege) {
        return privilegeMapper.addPrivilege(privilege);
    }

    @Override
    public int modifyPrivilege(Privilege privilege) {
        return privilegeMapper.modifyPrivilege(privilege);
    }

    @Override
    public int deletePrivilege(Privilege privilege) {
        return privilegeMapper.deletePrivilege(privilege);
    }

    @Override
    public int deletePrivileges(List<Privilege> privileges) {
        return privilegeMapper.deletePrivileges(privileges);
    }
}
