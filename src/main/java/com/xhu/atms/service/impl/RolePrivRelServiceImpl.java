package com.xhu.atms.service.impl;

import com.xhu.atms.dao.RolePrivRelMapper;
import com.xhu.atms.entity.RolePrivRel;
import com.xhu.atms.service.RolePrivRelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 */
@Service("rolePrivRelService")
public class RolePrivRelServiceImpl implements RolePrivRelService {
    @Resource(name = "rolePrivRelMapper")
    private RolePrivRelMapper rolePrivRelMapper;

    @Override
    public int countRolePrivRel(RolePrivRel rolePrivRel) {
        return rolePrivRelMapper.countRolePrivRel(rolePrivRel);
    }

    @Override
    public List<RolePrivRel> selectRolePrivRel(RolePrivRel rolePrivRel) {
        return rolePrivRelMapper.selectRolePrivRel(rolePrivRel);
    }

    @Override
    public int addRolePrivRel(RolePrivRel rolePrivRel) {
        return rolePrivRelMapper.addRolePrivRel(rolePrivRel);
    }

    @Override
    public int modifyRolePrivRel(RolePrivRel rolePrivRel) {
        return rolePrivRelMapper.modifyRolePrivRel(rolePrivRel);
    }

    @Override
    public int deleteRolePrivRel(RolePrivRel rolePrivRel) {
        return rolePrivRelMapper.deleteRolePrivRel(rolePrivRel);
    }
}
