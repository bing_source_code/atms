package com.xhu.atms.service.impl;

import com.xhu.atms.dao.NextCodeMapper;
import com.xhu.atms.service.NextCodeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by bingge on 2019/3/30.
 */
@Service("nextCodeService")
public class NextCodeServiceImpl implements NextCodeService {
    @Resource(name = "nextCodeMapper")
    private NextCodeMapper nextCodeMapper;

    @Override
    public long selectNextCode(String tableName) {
        return nextCodeMapper.selectNextCode(tableName);
    }
}
