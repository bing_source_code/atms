package com.xhu.atms.service.impl;

import com.xhu.atms.dao.AccRoleRelMapper;
import com.xhu.atms.entity.AccRoleRel;
import com.xhu.atms.service.AccRoleRelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/17.
 */
@Service("accRoleRelService")
public class AccRoleRelServiceImpl implements AccRoleRelService {
    @Resource(name = "accRoleRelMapper")
    private AccRoleRelMapper accRoleRelMapper;

    @Override
    public int countAccRoleRel(AccRoleRel accRoleRel) {
        return accRoleRelMapper.countAccRoleRel(accRoleRel);
    }

    @Override
    public List<AccRoleRel> selectAccRoleRel(AccRoleRel accRoleRel) {
        return accRoleRelMapper.selectAccRoleRel(accRoleRel);
    }

    @Override
    public int addAccRoleRel(AccRoleRel accRoleRel) {
        return accRoleRelMapper.addAccRoleRel(accRoleRel);
    }

    @Override
    public int modifyAccRoleRel(AccRoleRel accRoleRel) {
        return accRoleRelMapper.modifyAccRoleRel(accRoleRel);
    }

    @Override
    public int deleteAccRoleRel(AccRoleRel accRoleRel) {
        return accRoleRelMapper.deleteAccRoleRel(accRoleRel);
    }
}
