package com.xhu.atms.service.impl;

import com.xhu.atms.dao.CourseMapper;
import com.xhu.atms.entity.Course;
import com.xhu.atms.service.CourseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 */
@Service("courseService")
public class CourseServiceImpl implements CourseService {
    @Resource(name = "courseMapper")
    private CourseMapper courseMapper;

    @Override
    public int countCourse(Course course) {
        return courseMapper.countCourse(course);
    }

    @Override
    public List<Course> selectCourse(Course course) {
        return courseMapper.selectCourse(course);
    }

    @Override
    public int addCourse(Course course) {
        return courseMapper.addCourse(course);
    }

    @Override
    public int modifyCourse(Course course) {
        return courseMapper.modifyCourse(course);
    }

    @Override
    public int deleteCourse(Course course) {
        return courseMapper.deleteCourse(course);
    }

    @Override
    public int deleteCourses(List<Course> courses) {
        return courseMapper.deleteCourses(courses);
    }
}
