package com.xhu.atms.service.impl;

import com.xhu.atms.dao.AdminMapper;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Admin;
import com.xhu.atms.service.AdminService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/11.
 */
@Service("adminService")
public class AdminServiceImpl implements AdminService {
    @Resource(name = "adminMapper")
    private AdminMapper adminMapper;

    @Override
    public Account selectByCode(Account account) {
        return adminMapper.selectByCode(account);
    }

    @Override
    public int validateInfo(Account account) {
        return adminMapper.validateInfo(account);
    }

    @Override
    public int resetPassword(Account account) {
        return adminMapper.resetPassword(account);
    }

    @Override
    public Admin selectByAdminCode(Account account) {
        return adminMapper.selectByAdminCode(account);
    }

    @Override
    public int modifyPersonalInfo(Admin admin) {
        return adminMapper.modifyPersonalInfo(admin);
    }

    @Override
    public int modifyPassword(Account account) {
        return adminMapper.modifyPassword(account);
    }

    @Override
    public int countAccount(Admin admin) {
        return adminMapper.countAccount(admin);
    }

    @Override
    public List<Admin> selectAccount(Admin admin) {
        return adminMapper.selectAccount(admin);
    }

    @Override
    public int addAccount(Admin admin) {
        return adminMapper.addAccount(admin);
    }

    @Override
    public int deleteAccount(Admin admin) {
        return adminMapper.deleteAccount(admin);
    }

    @Override
    public int deleteAccounts(List<Admin> admins) {
        return adminMapper.deleteAccounts(admins);
    }
}
