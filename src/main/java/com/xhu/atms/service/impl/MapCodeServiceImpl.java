package com.xhu.atms.service.impl;

import com.xhu.atms.dao.MapCodeMapper;
import com.xhu.atms.entity.MapCode;
import com.xhu.atms.service.MapCodeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/4/2.
 */
@Service("mapCodeService")
public class MapCodeServiceImpl implements MapCodeService {
    @Resource(name = "mapCodeMapper")
    private MapCodeMapper mapCodeMapper;

    @Override
    public List<MapCode> selectMapCode(MapCode mapCode) {
        return mapCodeMapper.selectMapCode(mapCode);
    }
}
