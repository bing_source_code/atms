package com.xhu.atms.service.impl;

import com.xhu.atms.dao.EmailAcceptMapper;
import com.xhu.atms.entity.EmailAccept;
import com.xhu.atms.service.EmailAcceptService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/4/27.
 */
@Service("emailAcceptService")
public class EmailAcceptServiceImpl implements EmailAcceptService {
    @Resource(name = "emailAcceptMapper")
    private EmailAcceptMapper emailAcceptMapper;

    @Override
    public int countEmailAccept(EmailAccept emailAccept) {
        return emailAcceptMapper.countEmailAccept(emailAccept);
    }

    @Override
    public List<EmailAccept> selectEmailAccept(EmailAccept emailAccept) {
        return emailAcceptMapper.selectEmailAccept(emailAccept);
    }

    @Override
    public EmailAccept selectEmailAcceptByCode(EmailAccept emailAccept) {
        return emailAcceptMapper.selectEmailAcceptByCode(emailAccept);
    }

    @Override
    public int addEmailAccept(EmailAccept emailAccept) {
        return emailAcceptMapper.addEmailAccept(emailAccept);
    }

    @Override
    public int modifyEmailAccept(EmailAccept emailAccept) {
        return emailAcceptMapper.modifyEmailAccept(emailAccept);
    }

    @Override
    public int modifyEmailAcceptsStatus(List<EmailAccept> emailAccepts) {
        return emailAcceptMapper.modifyEmailAcceptsStatus(emailAccepts);
    }

    @Override
    public int deleteEmailAccept(EmailAccept emailAccept) {
        return emailAcceptMapper.deleteEmailAccept(emailAccept);
    }

    @Override
    public int deleteEmailAccepts(List<EmailAccept> emailAccepts) {
        return emailAcceptMapper.deleteEmailAccepts(emailAccepts);
    }
}
