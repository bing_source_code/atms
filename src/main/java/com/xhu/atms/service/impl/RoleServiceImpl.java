package com.xhu.atms.service.impl;

import com.xhu.atms.dao.RoleMapper;
import com.xhu.atms.entity.Role;
import com.xhu.atms.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
    @Resource(name = "roleMapper")
    private RoleMapper roleMapper;

    @Override
    public int countRole(Role role) {
        return roleMapper.countRole(role);
    }

    @Override
    public List<Role> selectRole(Role role) {
        return roleMapper.selectRole(role);
    }

    @Override
    public int addRole(Role role) {
        return roleMapper.addRole(role);
    }

    @Override
    public int modifyRole(Role role) {
        return roleMapper.modifyRole(role);
    }

    @Override
    public int deleteRole(Role role) {
        return roleMapper.deleteRole(role);
    }

    @Override
    public int deleteRoles(List<Role> roles) {
        return roleMapper.deleteRoles(roles);
    }
}
