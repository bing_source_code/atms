package com.xhu.atms.service.impl;

import com.xhu.atms.dao.TeacherMapper;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Teacher;
import com.xhu.atms.service.TeacherService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/14.
 */
@Service("teacherService")
public class TeacherServiceImpl implements TeacherService {
    @Resource(name = "teacherMapper")
    private TeacherMapper teacherMapper;

    @Override
    public Account selectByCode(Account account) {
        return teacherMapper.selectByCode(account);
    }

    @Override
    public int validateInfo(Account account) {
        return teacherMapper.validateInfo(account);
    }

    @Override
    public int resetPassword(Account account) {
        return teacherMapper.resetPassword(account);
    }

    @Override
    public Teacher selectByTeacherCode(Account account) {
        return teacherMapper.selectByTeacherCode(account);
    }

    @Override
    public int modifyPersonalInfo(Teacher teacher) {
        return teacherMapper.modifyPersonalInfo(teacher);
    }

    @Override
    public int modifyPassword(Account account) {
        return teacherMapper.modifyPassword(account);
    }

    @Override
    public int countAccount(Teacher teacher) {
        return teacherMapper.countAccount(teacher);
    }

    @Override
    public List<Teacher> selectAccount(Teacher teacher) {
        return teacherMapper.selectAccount(teacher);
    }

    @Override
    public int addAccount(Teacher teacher) {
        return teacherMapper.addAccount(teacher);
    }

    @Override
    public int deleteAccount(Teacher teacher) {
        return teacherMapper.deleteAccount(teacher);
    }

    @Override
    public int deleteAccounts(List<Teacher> teachers) {
        return teacherMapper.deleteAccounts(teachers);
    }
}
