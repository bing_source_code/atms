package com.xhu.atms.service.impl;

import com.xhu.atms.dao.AccountPrivilegesMapper;
import com.xhu.atms.entity.PrivilegeMenu;
import com.xhu.atms.service.AccountPrivilegesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/16.
 */
@Service("accountPrivilegesService")
public class AccountPrivilegesServiceImpl implements AccountPrivilegesService {
    @Resource(name = "accountPrivilegesMapper")
    private AccountPrivilegesMapper accountPrivilegesMapper;

    @Override
    public List<PrivilegeMenu> selectAccountPrivileges(long accId) {
        return accountPrivilegesMapper.selectAccountPrivileges(accId);
    }

    @Override
    public int selectLoginPrivilege(long accId) {
        return accountPrivilegesMapper.selectLoginPrivilege(accId);
    }
}
