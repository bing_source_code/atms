package com.xhu.atms.service.impl;

import com.xhu.atms.dao.StudentMapper;
import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Student;
import com.xhu.atms.service.StudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/3/14.
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService {
    @Resource(name = "studentMapper")
    private StudentMapper studentMapper;

    @Override
    public Account selectByCode(Account account) {
        return studentMapper.selectByCode(account);
    }

    @Override
    public int addStudent(Student student) {
        return studentMapper.addStudent(student);
    }

    @Override
    public int validateInfo(Account account) {
        return studentMapper.validateInfo(account);
    }

    @Override
    public int resetPassword(Account account) {
        return studentMapper.resetPassword(account);
    }

    @Override
    public Student selectByStudentCode(Account account) {
        return studentMapper.selectByStudentCode(account);
    }

    @Override
    public int modifyPersonalInfo(Student student) {
        return studentMapper.modifyPersonalInfo(student);
    }

    @Override
    public int modifyPassword(Account account) {
        return studentMapper.modifyPassword(account);
    }

    @Override
    public int countAccount(Student student) {
        return studentMapper.countAccount(student);
    }

    @Override
    public List<Student> selectAccount(Student student) {
        return studentMapper.selectAccount(student);
    }

    @Override
    public int deleteAccount(Student student) {
        return studentMapper.deleteAccount(student);
    }

    @Override
    public int deleteAccounts(List<Student> students) {
        return studentMapper.deleteAccounts(students);
    }
}
