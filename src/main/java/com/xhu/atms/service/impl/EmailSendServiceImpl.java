package com.xhu.atms.service.impl;

import com.xhu.atms.dao.EmailSendMapper;
import com.xhu.atms.entity.EmailSend;
import com.xhu.atms.service.EmailSendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by bingge on 2019/4/27.
 */
@Service("emailSendService")
public class EmailSendServiceImpl implements EmailSendService {
    @Resource(name = "emailSendMapper")
    private EmailSendMapper emailSendMapper;

    @Override
    public int countEmailSend(EmailSend emailSend) {
        return emailSendMapper.countEmailSend(emailSend);
    }

    @Override
    public List<EmailSend> selectEmailSend(EmailSend emailSend) {
        return emailSendMapper.selectEmailSend(emailSend);
    }

    @Override
    public EmailSend selectEmailSendByCode(EmailSend emailSend) {
        return emailSendMapper.selectEmailSendByCode(emailSend);
    }

    @Override
    public int addEmailSend(EmailSend emailSend) {
        return emailSendMapper.addEmailSend(emailSend);
    }

    @Override
    public int modifyEmailSend(EmailSend emailSend) {
        return emailSendMapper.modifyEmailSend(emailSend);
    }

    @Override
    public int modifyEmailSendsStatus(List<EmailSend> emailSends) {
        return emailSendMapper.modifyEmailSendsStatus(emailSends);
    }

    @Override
    public int deleteEmailSend(EmailSend emailSend) {
        return emailSendMapper.deleteEmailSend(emailSend);
    }

    @Override
    public int deleteEmailSends(List<EmailSend> emailSends) {
        return emailSendMapper.deleteEmailSends(emailSends);
    }
}
