package com.xhu.atms.service;

import com.xhu.atms.entity.Document;

import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 * 文档资料服务类
 */
public interface DocumentService {
    /**
     * 统计带条件的分页查询总数行
     * @param document：文档资料对象，包含条件
     * @return int：带条件的分页查询总数行
     */
    int countDocument(Document document);

    /**
     * 通过条件查询文档资料
     * @param document：文档资料对象，包含条件
     * @return List<Document> ：文档资料对象列表
     */
    List<Document> selectDocument(Document document);

    /**
     * 通过文档资料编号查询文档资料
     * @param document：文档资料对象，包含文档资料编号
     * @return List<Document> ：文档资料对象
     */
    Document selectDocumentByCode(Document document);

    /**
     * 添加文档资料
     * @param document：需要添加的文档资料对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addDocument(Document document);

    /**
     * 修改文档资料
     * @param document：文档资料对象，包含文档资料编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyDocument(Document document);

    /**
     * 删除文档资料，不会真的删除，只是将状态改为失效
     * @param document：文档资料对象，包含文档资料编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteDocument(Document document);

    /**
     * 删除文档资料，不会真的删除，只是将状态改为失效
     * @param documents：文档资料对象列表，包含文档资料编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteDocuments(List<Document> documents);
}
