package com.xhu.atms.service;

import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Admin;

import java.util.List;

/**
 * Created by bingge on 2019/3/11.
 * 管理员服务类
 */
public interface AdminService {
    /**
     * 通过管理员编号和密码查询管理员
     * @param account：账号对象，包含管理员编号和密码
     * @return Account：账号对象
     */
    Account selectByCode(Account account);

    /**
     * 验证管理员信息是否正确
     * @param account：账号对象，包含要验证的管理员姓名、编号、描述
     * @return int：1 成功，其他 失败
     */
    int validateInfo(Account account);

    /**
     * 重新设置管理员密码
     * @param account：账号对象，包含管理员编号和新密码
     * @return int：1 成功，其他 失败
     */
    int resetPassword(Account account);

    /**
     * 通过管理员编号查询管理员
     * @param account：账号对象，包含管理员编号
     * @return admin：管理员对象
     */
    Admin selectByAdminCode(Account account);

    /**
     * 通过管理员编号修改管理员的个人信息
     * @param admin：管理员对象，包含管理员编号及要修改的信息
     * @return int：受影响的行数
     */
    int modifyPersonalInfo(Admin admin);

    /**
     * 通过管理员编号和密码修改管理员密码
     * @param account：账号对象，包含管理员编号和密码
     * @return int：受影响的行数
     */
    int modifyPassword(Account account);

    /**
     * 统计带条件的分页查询总行数
     * @param admin：管理员对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countAccount(Admin admin);

    /**
     * 通过条件查询账号
     * @param admin：管理员对象，包含条件
     * @return List<Account> ：账号对象列表
     */
    List<Admin> selectAccount(Admin admin);

    /**
     * 添加账号
     * @param admin：需要添加的管理员对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addAccount(Admin admin);

    /**
     * 删除账号，不会真的删除，只是将状态改为失效
     * @param admin：管理员对象，包含账号编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteAccount(Admin admin);

    /**
     * 删除账号，不会真的删除，只是将状态改为失效
     * @param admins：管理员对象列表，包含账号编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteAccounts(List<Admin> admins);
}
