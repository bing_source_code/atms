package com.xhu.atms.service;

import com.xhu.atms.entity.EmailAccept;

import java.util.List;

/**
 * Created by bingge on 2019/4/27.
 */
public interface EmailAcceptService {
    /**
     * 统计带条件的分页查询总行数
     * @param emailAccept：接收邮件对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countEmailAccept(EmailAccept emailAccept);

    /**
     * 通过条件查询接收邮件
     * @param emailAccept：接收邮件对象，包含条件
     * @return List<EmailAccept> ：接收邮件对象列表
     */
    List<EmailAccept> selectEmailAccept(EmailAccept emailAccept);

    /**
     * 通过接收邮件编号查询发送邮件
     * @param emailAccept：接收邮件对象，包含发送邮件编号
     * @return List<EmailAccept> ：接收邮件对象
     */
    EmailAccept selectEmailAcceptByCode(EmailAccept emailAccept);

    /**
     * 添加接收邮件
     * @param emailAccept：需要添加的接收邮件对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addEmailAccept(EmailAccept emailAccept);

    /**
     * 修改接收邮件状态
     * @param emailAccepts：接收邮件对象列表，包含接收邮件编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int modifyEmailAcceptsStatus(List<EmailAccept> emailAccepts);

    /**
     * 修改接收邮件
     * @param emailAccept：接收邮件对象，包含接收邮件编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyEmailAccept(EmailAccept emailAccept);

    /**
     * 删除接收邮件，不会真的删除，只是将状态改为删除
     * @param emailAccept：接收邮件对象，包含接收邮件编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteEmailAccept(EmailAccept emailAccept);

    /**
     * 删除接收邮件，不会真的删除，只是将状态改为删除
     * @param emailAccepts：接收邮件对象列表，包含接收邮件编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteEmailAccepts(List<EmailAccept> emailAccepts);
}
