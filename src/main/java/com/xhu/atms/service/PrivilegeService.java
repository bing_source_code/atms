package com.xhu.atms.service;

import com.xhu.atms.entity.Privilege;

import java.util.List;

/**
 * Created by bingge on 2019/3/27.
 * 权限服务类
 */
public interface PrivilegeService {
    /**
     * 统计带条件的分页查询总数行
     * @param privilege：权限对象，包含条件
     * @return int：带条件的分页查询总数行
     */
    int countPriv(Privilege privilege);

    /**
     * 通过条件查询权限
     * @param privilege：权限对象，包含条件
     * @return List<Privilege> ：权限对象列表
     */
    List<Privilege> selectPriv(Privilege privilege);

    /**
     * 添加权限
     * @param privilege：需要添加的权限对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addPrivilege(Privilege privilege);

    /**
     * 修改权限
     * @param privilege：权限对象，包含权限编号及要修改的信息
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int modifyPrivilege(Privilege privilege);

    /**
     * 删除权限，不会真的删除，只是将状态改为失效
     * @param privilege：权限对象，包含权限编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deletePrivilege(Privilege privilege);

    /**
     * 删除权限，不会真的删除，只是将状态改为失效
     * @param privileges：权限对象列表，包含权限编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deletePrivileges(List<Privilege> privileges);
}
