package com.xhu.atms.service;

import com.xhu.atms.entity.MapCode;
import java.util.List;

/**
 * Created by bingge on 2019/3/16.
 * 码值服务类
 */
public interface MapCodeService {
    /**
     * 通过列名和表名查询map_code表
     * @param mapCode：码值对象，包含列名和表名
     * @return List<MapCode>：码值对象列表
     */
    List<MapCode> selectMapCode(MapCode mapCode);
}
