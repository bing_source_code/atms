package com.xhu.atms.service;

/**
 * Created by bingge on 2019/3/30.
 */
public interface NextCodeService {
    /**
     * 查询编号自动增加的表的下一个编号
     * @param tableName：表名
     * @return long：下一个编号
     */
    long selectNextCode(String tableName);
}
