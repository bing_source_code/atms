package com.xhu.atms.service;

import com.xhu.atms.entity.Account;
import com.xhu.atms.entity.Teacher;

import java.util.List;

/**
 * Created by bingge on 2019/3/14.
 * 教师服务类
 */
public interface TeacherService {
    /**
     * 通过教师编号和密码查询教师
     * @param account：账号对象，包含教师编号和密码
     * @return Account：账号对象
     */
    Account selectByCode(Account account);

    /**
     * 验证教师信息是否正确
     * @param account：账号对象，包含要验证的教师姓名、编号、描述
     * @return int：1 成功，其他 失败
     */
    int validateInfo(Account account);

    /**
     * 重新设置教师密码
     * @param account：账号对象，包含教师编号和新密码
     * @return int：1 成功，其他 失败
     */
    int resetPassword(Account account);

    /**
     * 通过教师编号查询学生
     * @param account：账号对象，包含教师编号
     * @return teacher：教师对象
     */
    Teacher selectByTeacherCode(Account account);

    /**
     * 通过教师编号修改教师的个人信息
     * @param teacher：教师对象，包含教师编号及要修改的信息
     * @return int：受影响的行数
     */
    int modifyPersonalInfo(Teacher teacher);

    /**
     * 通过教师编号和密码修改教师密码
     * @param account：账号对象，包含教师编号和密码
     * @return int：受影响的行数
     */
    int modifyPassword(Account account);

    /**
     * 统计带条件的分页查询总行数
     * @param teacher：教师对象，包含条件
     * @return int：带条件的分页查询总行数
     */
    int countAccount(Teacher teacher);

    /**
     * 通过条件查询账号
     * @param teacher：教师对象，包含条件
     * @return List<Account> ：账号对象列表
     */
    List<Teacher> selectAccount(Teacher teacher);

    /**
     * 添加账号
     * @param teacher：需要添加的教师对象
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int addAccount(Teacher teacher);

    /**
     * 删除账号，不会真的删除，只是将状态改为失效
     * @param teacher：教师对象，包含账号编号
     * @return int：受影响的行数，1 成功，其他 失败
     */
    int deleteAccount(Teacher teacher);

    /**
     * 删除账号，不会真的删除，只是将状态改为失效
     * @param teachers：教师对象列表，包含账号编号
     * @return int：受影响的行数，列表的元素个数 成功，其他 失败
     */
    int deleteAccounts( List<Teacher> teachers);
}
